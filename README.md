# CHI-KNOW-PO/Library

## Description

This project aims to digitize, edit and publish a corpus of ALTO files. Our case study includes poetic anthologies, commentaries, dictionaries, encyclopedias and practical treatises from the Chinese medieval period (ca. 200-1000).

## Installation

1. Create conda environment and activate it:
```bash
conda create --name chi-know-po-library python=3.12 -y && \
conda activate chi-know-po-library
```

2. Install [Poetry](https://python-poetry.org/) and project dependencies:
```bash
conda install -c conda-forge poetry -y && \
poetry install
```

3. Set python path to source folder
```bash
export PYTHONPATH=$(pwd)/src
```

## Contributing

```bash
# add a dependency
poetry add <package>

# remove a dependency
poetry remove <package>
```
