leader	02005nam a2200361   450 
001	990005127710107166
005	20230920104747.0
035	##$a000512718 
035	##$aCDF01000512718 
035	##$a(Aleph)000512771CDF01 
100	##$a20100906d1862----k  y0frey50      ea 
101	0#$achi 
105	##$a    z   00 
106	##$ar 
200	1#$6a01 $7ea $a毛詩草木鳥獸蟲魚疏 二卷 $f[吳] 陸機 撰; [清] 丁晏 校正 
200	1#$6a01 $7ba $aMaoshi caomu niaoshou chongyu shu $fLu Ji; Ding Yan 
205	##$6a02 $7ea $a清刊本, 清咸豐七年[1857]序刊 
210	##$6a03 $7ea $a山陽 $c丁氏六藝堂 $d清同治元年 [1862] 
210	##$6a03 $7ba $aShanyang $cDingshi liuyi tang $d1862 
215	##$a1 fasc. $d18,5x13,2cm 
225	1#$6a04 $7ea $a頤志齋叢書 $f[清] 丁晏 輯撰 $h第一函第五冊 
225	1#$6a04 $7ba $aYizhi zhai congshu $fDing Yan 
300	##$a一冊; 白口左右雙邊, 單魚尾, 半頁十行, 一行二十字, 小字雙行, 框高18,5厘米, 寬13,2厘米 
410	##$6a30 $7ea $t頤志齋叢書 $f[清] 丁晏 輯撰 $h第一函第五冊 
410	##$6a30 $7ba $tYizhi zhai congshu $fDing Yan 
610	01$alittérature $aclassiques $apoésie $ahist. naturelle $abotanique $aChine $aQing $2CHI 
700	#1$6a07 $7ea $a陸機 $f[吳] 
700	#1$6a07 $7ba $aLu $bJi 
701	#1$6a06 $7ea $a丁晏 $f[清] 
701	#1$6a06 $7ba $aDing $bYan 
801	#0$aFR $bCDF 
936	##$aC-4369-6 
990	##$aV I 111 (1) 5 $cIHEC-chinois $wCC 
