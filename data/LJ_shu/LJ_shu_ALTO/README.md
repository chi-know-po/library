# About-Plants

The CHI-KNOW-PO project aims to identify patterns of lexical field co-occurrences and textual borrowings across genres in Medieval China. It focuses more specifically on plants among other topics.

## About the project

The project is three-folded:
1. The project aims to digitize, edit and publish a corpus of poetic anthologies, commentaries, dictionaries, encyclopedias and practical treatises from the Chinese medieval period (*ca.* 200-1000);
2. The project builds a database with bio-bibiographic information along with word semantic information (first occurrence, semantic group identity, equivalents rather than synonyms) useful for editorial as well as text mining purposes;
3. The project develops a number of scripts that allow one to perform customized searches, filtering and statistical analyses to study co-occurrence of words and word groups in given texts and identify possible cases of text reuse.

As of October 2022, the team releases a first version of its database and scripts to identify co-occurrences.

In the future we plan to expand to be able to perform diachronic analyses of changes in word usage as well as create a number of visualisations. We also plan to build a new tool to identify similarities between texts.

The project is written in Python based on Jupyter notebooks and uses SQL for the database.

However, we aim to organise the code so that people with little programming experience could make use of it. If there is something not clear from the documentation or if you have any suggestions, you are welcome to write to us using contact information from [Contributors](## Contributors) section.

Below is a short introduction on how to use the notebooks. For more information please see the Wiki.

## Project structure

The repository is composed of the following items:
- corpus (as of October 2022, simply a sample corpus),
- database (a dump of the CKP SQL database which is also remotely installed so as to allow our scripts to query it),
- scripts
  - with the main script to track cooccurrences entitled "collocations.ipynb",
  - with the modules used by this script.

## Installation

If you do not have anaconda installed on your computer, follow the instructions [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) to install it.

Download the contents of this repository using git or directly from this page:
- [instructions for cloning or downloading in Gitlab](https://docs.gitlab.com/ee/user/project/repository/),
- [instructions for Github](https://docs.github.com/en/repositories/creating-and-managing-repositories/cloning-a-repository)

Install the environment that is stored in the root folder: ckp_env.yml Do this by navigating to the folder in the terminal and running the command:

```
conda env create -f ckp_env.yml
```

More information on how to install environments:

- [Creating an environment in conda using a .yml file](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)
- [Make new environment show in jupyter notebooks (see method 2)](https://towardsdatascience.com/get-your-conda-environment-to-show-in-jupyter-notebooks-the-easy-way-17010b76e874)

## How to start

The main working file of the project is collocations.ipynb.
It contains two parts:
1. Collocation finder: used to set the corpus and analysis settings, extract information from the corpus, perform statistical analyses of the collocations and save the results to an external file.
2. Pair explorer: used to display the information from the previous notebook. Takes the list of texts that contain the words of interest set in Collocations.ipynb and displays them alongside with basic statistics, allowing to add or exclude extra words from the results – the main feature is being able to search for texts that contain synonyms of the target words.

We suggest starting with this notebook. More detailed explanation can be found in wiki.

## Contributors

- Marie BIZAIS-LILLIG (bizais@unistra.fr): Principal Investigator
- HU Xinmin: XML-TEI text editing, development and visualization
- LIAO Shueh-Ying: Database creation and maintenance, XML-TEI named entities automatic tagging and HTR
- Tilman SCHALMEY: Development and database creation
- Mariana ZORKINA (marianazorkina@gmail.com): Development and maintenance

CHI-KNOW-PO collaborates with:
- Four French Libraries (BNU, BULAC, IHEC, Bibliothèque de l'Institut d'études chinoises de l'Université de Strasbourg),
- CALFA Team (Start up company specialized in HTR)

## Support

The CHI-KNOW-PO Project is funded by:
- COLLEX (Collections of Excellence funding support)
- The Institute for Advanced Studies of the University of Strasbourg (USIAS)
- UR 1340- GÉO University of Strasbourg Research Laboratory
