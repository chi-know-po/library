# Data

## Description

This is the sample data to demonstrate the usage of our ALTO to TEI transformation.

## Structure

- **files4repo**: These are the book pages pictures before being transformed as ALTO files.
- **Altos_Samples**: Contains ALTO files.
- **header_doc**: Contains models of expected header values for the TEI.
- **LJ_shu**: These contains specific informations (metadata, structure, edition information) for the book *Mao Shi caomu niaoshou chongyu shu* (毛詩草木鳥獸蟲魚疏).
