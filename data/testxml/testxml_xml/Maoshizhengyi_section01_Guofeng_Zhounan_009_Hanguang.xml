<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風周南之漢廣（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Hanguang in the Zhounan section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">10.34847/nkl.a9dbfxhu#4d1d7c18effc97a579008d5432ba3c06d95c1c8c</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="281" to="282"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷一</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷一</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80140&amp;amp;page=96</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj009" type="preface">
        <ab resp="#Mao" xml:id="sj009_com_Mao">《漢廣》，德廣所及也。文王之道被於南國，美化行乎江漢之域，無思犯禮，求而不可得也。</ab>
        <ab resp="#ZX" xml:id="sj009_com_ZX">紂時淫風遍於天下，維江、漢之域先受文王之教化。</ab>
        <ab resp="#LDM" xml:id="sj009_com_LDM">漢廣，漢水名也。《尚書》云：「嶓塚導漾水，東流為漢。」被，皮義反。紂，直九反。殷王也。遍，邊見反。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj009 #sj009_com_Mao">
          <head>［疏］「《漢廣》三章，章八句」至「不可得」。正義曰：</head>
          <ab>作《漢廣》詩者，言德廣所及也。言文王之道，初致《桃夭》、《芣苡》之化，今被於南國，美化行於江、漢之域，故男無思犯禮，女求而不可得，此由德廣所及然也。此與《桃夭》皆文王之化，後妃所贊，於此言文王者，因經陳江、漢，指言其處為遠，辭遂變後妃而言文王，為遠近積漸之義。敘於此既言德廣，《汝墳》亦廣可知，故直云「道化行」耳。此既言美化，下篇不嫌不美，故直言「文王之化」，不言美也。言南國則六州，猶《羔羊序》云「召南之國」也。彼言召南，此不言周南者，以天子事廣，故直言南。彼論諸侯，故止言召南之國。此「無思犯禮，求而不可得」，總序三章之義也。</ab>
        </div>
        <div source="#sj009_com_ZX">
          <head>箋「紂時」至「教化」。正義曰：</head>
          <ab>言先者，以其餘三州未被文王之化，故以江、漢之域為先被也。定本「先被」作「先受」，因經、序有江、漢之文，故言之耳。其實六州共被文王之化，非江、漢獨先也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj009">
      <lg type="stanza" xml:id="sj009s01">
        <lg type="couplet" xml:id="sj009s01e1">
          <l xml:id="sj009s01l1">南有喬木ㆍ</l>
          <l xml:id="sj009s01l2">不可/方思/息ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s01e2">
          <l xml:id="sj009s01l3">漢有游女ㆍ</l>
          <l xml:id="sj009s01l4">不可求思ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s01e3">
          <l xml:id="sj009s01l5">漢之廣矣ㆍ</l>
          <l xml:id="sj009s01l6">不可泳思ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s01e4">
          <l xml:id="sj009s01l7">江之永矣ㆍ</l>
          <l xml:id="sj009s01l8">不可方思ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj009s02">
        <lg type="couplet" xml:id="sj009s02e1">
          <l xml:id="sj009s02l1">翹翹錯薪ㆍ</l>
          <l xml:id="sj009s02l2">言刈其楚ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s02e2">
          <l xml:id="sj009s02l3">之子于歸ㆍ</l>
          <l xml:id="sj009s02l4">言秣其馬ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s02e3">
          <l xml:id="sj009s02l5">漢之廣矣ㆍ</l>
          <l xml:id="sj009s02l6">不可泳思ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s02e4">
          <l xml:id="sj009s02l7">江之永矣ㆍ</l>
          <l xml:id="sj009s02l8">不可方思ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj009s03">
        <lg type="couplet" xml:id="sj009s03e1">
          <l xml:id="sj009s03l1">翹翹錯薪ㆍ</l>
          <l xml:id="sj009s03l2">言刈其蔞ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s03e2">
          <l xml:id="sj009s03l3">之子于歸ㆍ</l>
          <l xml:id="sj009s03l4">言秣其駒ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s03e3">
          <l xml:id="sj009s03l5">漢之廣矣ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj009s03e4">
          <l xml:id="sj009s03l6">江之永矣ㆍ</l>
          <l xml:id="sj009s03l7">不可方思ㆍ</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj009s01" type="commentaries">
      <div source="#sj009s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj009s01e1_com_Mao">
          <ab>興也。南方之木，美喬上竦也。思，辭也。漢上游女，無求思者。</ab>
        </div>
        <div resp="#ZX" xml:id="sj009s01e1_com_ZX">
          <head>箋云：</head>
          <ab>不可者，本有可道也。木以高其枝葉之故，故人不得就而止息也。興者，喻賢女雖出游流水之上，人無欲求犯禮者，亦由貞潔使之然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj009s01e1_com_LDM">
          <ab>喬木，亦作「橋」，渠驕反，徐又紀橋反。休息並如字，古本皆爾，本或作「休思」，此以意改爾。竦，粟勇反。「流水」，本或作「漢水」。</ab>
        </div>
      </div>
      <div source="#sj009s01e3" type="commentary">
        <div resp="#Mao" xml:id="sj009s01e3_com_Mao">
          <ab>潛行為泳。永，長。方，泭也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj009s01e3_com_ZX">
          <head>箋云：</head>
          <ab>漢也，江也，其欲渡之者，必有潛行乘泭之道。今以廣長之故，故不可也。又喻女之貞潔，犯禮而往，將不至也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj009s01e3_com_LDM">
          <ab>泳音詠。泭，芳於反，本亦作「水符」，又作「桴」，或作「柎」，並同。沈旋音附。《方言》云：「泭謂之𥴖，𥴖謂之筏。筏，秦、晉通語也。」孫炎注《爾雅》云：「方木置水為柎筏也。郭璞云：「水中𥴖筏也。」又云：「木曰𥴖，竹曰筏，小筏曰泭。」𥴖音皮隹反。柎、筏同音伐。樊光《爾雅》本作「柎」。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj009s01e1_com_Mao">
          <head>傳「思辭」至「思者」。正義曰：</head>
          <ab>以泳思、方思之等皆不取思為義，故為辭也。經「求思」之文在「游女」之下，傳解「喬木」之下，先言「思，辭」，然後始言「漢上」，疑經「休息」之字作「休思」也。何則？詩之大體，韻在辭上，疑休、求字為韻，二字俱作「思」，但未見如此之本，不敢輒改耳。《內則》云：「女子居內，深宮固門。」此漢上有游女者，《內則》言「閽寺守之」，則貴家之女也。庶人之女，則執筐行饁，不得在室，故有出游之事。既言不可求，明人無求者。定本「喬上竦」，無木字。</ab>
        </div>
        <div source="#sj009s01e1_com_ZX">
          <head>箋「不可」至「之然」。正義曰：</head>
          <ab>箋知此為「本有可道」者，以此皆據男子之辭，若恆不可，則不應發「不可」之辭，故云「本有可道」也。此箋與下箋互也。此直言不可者，本有可道，總解經「不可」之文，遂略木有可息之道。箋下言渡江、漢有潛行、乘泭之道，不釋「不可」之文，是其互也。然本淫風大行之時，女有可求，今被文王之化，游女皆潔。此云潔者，本未必已淫，興者取其一象，木可就蔭，水可方、泳，猶女有可求。今木以枝高不可休息，水以廣長不可求渡，不得要言木本小時可息，水本一勺可渡也。言「木以高其枝葉」，解傳言「上竦」也。言女雖出游漢水之上者，對不出不游者言。無求犯禮者，謂男子無思犯禮，由女貞潔使之然也。所以女先貞而男始息者，以奸淫之事皆男唱而女和。由禁嚴於女，法緩於男，故男見女不可求，方始息其邪意。《召南》之篇，女既貞信，尚有強暴之男是也。</ab>
        </div>
        <div source="#sj009s01e2_com_Mao">
          <head>傳「潛行」至「方泭」。正義曰：</head>
          <ab>「潛行為泳」，《釋水》文。郭璞曰：「水底行也。」《晏子春秋》曰：潛行逆流百步，順流七里。「永，長」，《釋詁》文。「方，泭」，《釋言》文。孫炎曰：「方，水中為泭筏也。」《論語》曰：「乘桴浮於海。」注云：「桴，編竹木，大曰筏，小曰桴。」是也。</ab>
        </div>
        <div source="#sj009s01e2_com_ZX">
          <head>箋「漢也」至「不至」。正義曰：</head>
          <ab>此江漢、之深，不可乘泭而渡。《穀風》云「就其深矣，方之舟之」者，雖深，不長於江、漢故也。言「將不至」者，雖求之，女守禮，將不肯至也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj009s02" type="commentaries">
      <div source="#sj009s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj009s02e1_com_Mao">
          <ab>翹翹，薪貌。錯，雜也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj009s02e1_com_ZX">
          <head>箋云：</head>
          <ab>楚，雜薪之中尤翹翹者。我欲刈取之，以喻眾女皆貞潔，我又欲取其尤高潔者。</ab>
        </div>
        <div resp="#LDM" xml:id="sj009s02e1_com_LDM">
          <ab>翹，祁遙反，沈其堯反。「尤高潔」者，一本無「潔」字。</ab>
        </div>
      </div>
      <div source="#sj009s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj009s02e2_com_Mao">
          <ab>秣，養也。六尺以上曰馬。</ab>
        </div>
        <div resp="#ZX" xml:id="sj009s02e2_com_ZX">
          <head>箋云：</head>
          <ab>之子，是子也。謙不敢斥其適己，於是子之嫁，我原秣其馬，致禮餼，示有意焉。</ab>
        </div>
        <div resp="#LDM" xml:id="sj009s02e2_com_LDM">
          <ab>秣，莫葛反。《說文》云：「食馬穀也。」上，時掌反，下文同。餼，虛氣反，牲腥曰餼。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary"/>
    </div>
    <div source="#sj009s03" type="commentaries">
      <div source="#sj009s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj009s03e1_com_Mao">
          <ab>蔞，草中之翹翹然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj009s03e1_com_LDM">
          <ab>蔞，力俱反，馬云：「蔞，蒿也。」郭云：「似艾。」音力侯反。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《漢廣》三章，章八句。</ab>
  </back>
</text>
</TEI>
