<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風周南之汝墳（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Rufen in the Zhounan section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">10.34847/nkl.a9dbfxhu#2e1730670d8a233800d8d8670578ffa541d0d7c1</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="282" to="283"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷一</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷一</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80140&amp;amp;page=103</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj010" type="preface">
        <ab resp="#Mao" xml:id="sj010_com_Mao">《汝墳》，道化行也。文王之化行乎汝墳之國，婦人能閔其君子，猶勉之以正也。</ab>
        <ab resp="#ZX" xml:id="sj010_com_ZX">言此婦人被文王之化，厚事其君子。</ab>
        <ab resp="#LDM" xml:id="sj010_com_LDM">汝墳，符云反。《常武》傳云：墳，涯也。能閔，密謹反，傷念也。一本有「婦人」二字。被，皮義反。</ab>
      </div>
      <div resp="#KYD" type="subcommentary"/>
    </div>
    <div type="poem" xml:id="sj010">
      <lg type="stanza" xml:id="sj010s01">
        <lg type="couplet" xml:id="sj010s01e1">
          <l xml:id="sj010s01l1">遵彼汝墳ㆍ</l>
          <l xml:id="sj010s01l2">伐其條枚ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj010s01e2">
          <l xml:id="sj010s01l3">未見君子ㆍ</l>
          <l xml:id="sj010s01l4">惄如調飢ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj010s02">
        <lg type="couplet" xml:id="sj010s02e1">
          <l xml:id="sj010s02l1">遵彼汝墳ㆍ</l>
          <l xml:id="sj010s02l2">伐其條肄ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj010s02e2">
          <l xml:id="sj010s02l3">既見君子ㆍ</l>
          <l xml:id="sj010s02l4">不我遐棄ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj010s03">
        <lg type="couplet" xml:id="sj010s03e1">
          <l xml:id="sj010s03l1">魴魚赬尾ㆍ</l>
          <l xml:id="sj010s03l2">王室如燬ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj010s03e2">
          <l xml:id="sj010s03l3">雖則如燬ㆍ</l>
          <l xml:id="sj010s03l4">父母孔邇ㆍ</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj010s01" type="commentaries">
      <div source="#sj010s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj010s01e1_com_Mao">
          <ab>遵，循也。汝，水名也。墳，大防也。枝曰條，乾曰枚。</ab>
        </div>
        <div resp="#ZX" xml:id="sj010s01e1_com_ZX">
          <head>箋云：</head>
          <ab>伐薪於汝水之側，非婦人之事，以言己之君子賢者，而處勤勞之職，亦非其事。</ab>
        </div>
        <div resp="#LDM" xml:id="sj010s01e1_com_LDM">
          <ab>枚，妹回反，乾也。</ab>
        </div>
      </div>
      <div source="#sj010s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj010s01e2_com_Mao">
          <ab>惄，饑意也。調，朝也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj010s01e2_com_ZX">
          <head>箋云：</head>
          <ab>惄，思也。未見君子之時，如朝饑之思食。</ab>
        </div>
        <div resp="#LDM" xml:id="sj010s01e2_com_LDM">
          <ab>{紂心}本又作「惄」，乃歷反，《韓詩》作「溺」，音同。調，張留反，又作「輖」，音同。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj010s01">
          <head>［疏］「遵彼」至「調饑」。正義曰：</head>
          <ab>言大夫之妻，身自循彼汝水大防之側，伐其條枝枚乾之薪。以為己伐薪汝水之側，非婦人之事，因閔己之君子賢者，而處勤勞之職，亦非其事也。既閔其勞，遂思念其事，言己未見君子之時，我之思君子，惄然如朝饑之思食也。</ab>
        </div>
        <div source="#sj010s01e1_com_Mao">
          <head>傳「汝水」至「曰枚」。正義曰：</head>
          <ab>《釋水》云：「汝為濆。」傳曰「濟汝」，故知是水名也。「墳，大防」，《釋丘》文。李巡曰：「墳謂崖岸狀如墳墓，名大防也。」故《常武》傳曰：「墳，崖。」《大司徒》注云：「水崖曰墳。」則此墳謂汝水之側崖岸大防也。若然，《釋水》云「水自河出為灉，江為沱」，別為小水之名。又云：「江有沱，河有灉，汝有濆。」李巡曰：「江、河、汝旁有肥美之地名。」郭璞曰：「《詩》云遵彼汝濆」，則郭意以此汝墳為濆，汝所分之處有美地，因謂之濆。箋、傳不然者，以彼濆從水，此墳從土，且伐薪宜於崖岸大防之上，不宜在濆汝之間故也。枝曰條，乾曰枚，無文也。以枚非木，則條亦非木，明是枝幹相對為名耳。枝者木大，不可伐其干，取條而已。枚，細者，可以全伐之也。《周禮》有《銜枚氏》，注云「枚狀如箸」，是其小也。《終南》云：「有條有梅。」文與梅連，則條亦木名也。故傳曰「條、槄」，與此異也。下章言「條肄」，肄，餘也，斬而復生，是為餘也，如今蘗生者，亦非木名也。襄二十九年《左傳》曰：「晉國不恤宗周之闕，而夏肄是屏。」又曰：「杞，夏餘也。」是肄為復生之餘。</ab>
        </div>
        <div source="#sj010s01e1_com_ZX">
          <head>箋「伐薪」至「其事」。正義曰：</head>
          <ab>知婦人自伐薪者，以序云「婦人能閔其君子」，則閔其君子者，是汝墳之國婦人也。經言「遵彼汝墳」，故知婦人自伐薪也。大夫之妻，尊為命婦，而伐薪者，由世亂時勞，君子不在。猶非其宜，故云非婦人之事。婦人之事，深宮固門，紡績織紝之謂也。不賢而勞，是其常，故以賢者處勤為非其事也。</ab>
        </div>
        <div source="#sj010s01e2_com_Mao">
          <head>傳「惄，饑意」。箋「惄，思」。正義曰：</head>
          <ab>《釋詁》云：「惄，思也。」舍人曰：「惄，志而不得之思也。」《釋言》云：「惄，饑也。」李巡曰：「惄，宿不食之饑也。」然則惄之為訓，本為思耳。但饑之思食，意又惄然，故又以為饑。惄是饑之意，非饑之狀，故傳言「饑意」。箋以為思，義相接成也。此連調饑為文，故傳以為饑意。《小弁》云「惄焉如搗」，無饑事，故箋直訓為「思也」。此以思食比思夫，故箋又云：「如朝饑之思食。」</ab>
        </div>
      </div>
    </div>
    <div source="#sj010s02" type="commentaries">
      <div source="#sj010s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj010s02e1_com_Mao">
          <ab>肄，餘也。斬而復生曰肄。</ab>
        </div>
        <div resp="#LDM" xml:id="sj010s02e1_com_LDM">
          <ab>肄，以自反。沈云：「徐音以世反，非。」復，扶富反。</ab>
        </div>
      </div>
      <div source="#sj010s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj010s02e2_com_Mao">
          <ab>既，已。遐，遠也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj010s02e2_com_ZX">
          <head>箋云：</head>
          <ab>己見君子，君子反也，於已反得見之，知其不遠棄我而死亡，於思則愈，故下章而勉之。</ab>
        </div>
        <div resp="#LDM" xml:id="sj010s02e2_com_LDM">
          <ab>思，如字，又息嗣反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj010s02e2">
          <head>［疏］「既見君子，不我遐棄」。正義曰：</head>
          <ab>不我遐棄，猶云不遐棄我。古之人語多倒，《詩》之此類眾矣。婦人以君子處勤勞之職，恐避役死亡，今思之，覬君子事訖得反。我既得見君子，即知不遠棄我而死亡，我於思則愈。未見，恐其逃亡；既見，知其不死，故憂思愈也。</ab>
        </div>
        <div source="#sj010s02e2_com_ZX">
          <head>箋「已見」至「勉之」。正義曰：</head>
          <ab>言不遠棄我，我者，婦人自謂也。若君子死亡，已不復得見，為遠棄我。今不死亡，已得見之，為不遠棄我也。然君子或不堪其苦，避役死亡；或自思公義，不避勞役，不由於婦人，然婦人閔夫之辭，據婦人而言耳。鄭知不直遠棄己而去，知為王事死亡者，以閔其勤勞，豈為棄己而憂也。下章云「父母孔邇」，是勉勸之辭，由此畏其死亡，故下章勉之。定本箋之下云「己見君子，君子反也，於己反得見之」，俗本多不然。</ab>
        </div>
      </div>
    </div>
    <div source="#sj010s03" type="commentaries">
      <div source="#sj010s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj010s03e1_com_Mao">
          <ab>赬，赤也，魚勞則尾赤。毀，火也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj010s03e1_com_ZX">
          <head>箋云：</head>
          <ab>君子仕於亂世，其顏色瘦病，如魚勞則尾赤。所以然者，畏王室之酷烈。是時紂存。</ab>
        </div>
        <div resp="#LDM" xml:id="sj010s03e1_com_LDM">
          <ab>魴，符方反，魚名。赬，敕貞反，《說文》作「䞓」，又作「赬」，並同。毀音毀，齊人謂火曰毀。郭璞又音貨。字書作「尾」，音毀，《說文》同。一音火尾反。或云：楚人名曰燥，齊人曰毀，吳人曰尾，此方俗訛語也。瘦，色救反。酷，苦毒反。</ab>
        </div>
      </div>
      <div source="#sj010s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj010s03e2_com_Mao">
          <ab>孔，甚。邇，近也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj010s03e2_com_ZX">
          <head>箋云：</head>
          <ab>闢此勤勞之處，或時得罪，父母甚近，當念之，以免於害，不能為疏遠者計也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj010s03e2_com_LDM">
          <ab>「闢此」，一本作「辭此」。處，昌慮反。為疏，於偽反。疏亦作疏。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj010s03">
          <head>［疏］「魴魚」至「孔邇」。正義曰：</head>
          <ab>婦人言魴魚勞則尾赤，以興君子苦則容悴。君子所以然者，由畏王室之酷烈猛熾如火故也。既言君子之勤苦，即勉之，言今王室之酷烈雖則如火，當勉力從役，無得逃避。若其避之，或時得罪，父母甚近，當自思念，以免於害，無得死亡，罪及父母，所謂勉之以正也。</ab>
        </div>
        <div source="#sj010s03e1_com_Mao">
          <head>傳「赬，赤」至「毀火」。正義曰：</head>
          <ab>《釋器》云：「再染謂之赬。」郭云：「赬，淺赤也。」魴魚之尾不赤，故知勞則尾赤。哀十七年《左傳》曰：「如魚赬尾，衡流而徬徉。」鄭氏云：魚肥則尾赤，以喻蒯瞆淫縱。不同者，此自魴魚尾本不赤，赤故為勞也。鄭以為彼言徬徉為魚肥，不指魚名，猶自有肥而尾赤者。服氏亦為魚勞。「毀，火」，《釋言》文也。李巡曰：「毀一名火。」孫炎曰：「方言有輕重，故謂火為毀也。」</ab>
        </div>
        <div source="#sj010s03e1_com_ZX">
          <head>箋「君子」至「紂存」。正義曰：</head>
          <ab>言君子仕於亂世，不斥大夫士。王肅云：「當紂之時，大夫行役。」王基云：「汝墳之大夫久而不歸。」樂詳、馬昭、孔晁、孫毓等皆云大夫，則箋云仕於亂世，是為大夫矣。若庶人之妻，《杕杜》言「我心傷悲」，《伯兮》則云「甘心首疾」，憂思昔在於情性，豈有勸以德義，恐其死亡若是乎！序稱「勉之以正」，則非庶人之妻。言賢者不宜勤勞，則又非為士，《周南》、《召南》，述本大同，而《殷其雷》召南之大夫遠行從政，其妻勸以義。此引父母之甚近，傷王室之酷烈，閔之則恐其死亡，勉之則勸其盡節，比之於《殷其雷》，志遠而義高，大夫妻於是明矣。雖王者之風，見感文王之化，但時實紂存，文王率諸侯以事殷，故汝墳之國，大夫猶為殷紂所役。若稱王以後，則不復事紂，六州，文王所統，不為紂役也。箋以二《南》文王之事，其衰惡之事，舉紂以明之。上《漢廣》云「求而不可得」，本有可得之時，言紂時淫風大行。此云「王室如毀」，言是時紂存。《行露》云「衰亂之俗微」，言紂末之時，《野有死麇》云「惡無禮」，言紂時之世。《麟趾》有「衰世之公子」，不言紂時。法有詳略，承此可知也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《汝墳》三章，章四句。</ab>
  </back>
</text>
</TEI>
