<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風周南之兔罝（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Tuju in the Zhounan section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">10.34847/nkl.a9dbfxhu#8dbb0218f5bd4c804b792c222946d2e99ee2a0c5</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="281" to="281"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷一</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷一</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80140&amp;amp;page=88</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj007" type="preface">
        <ab resp="#Mao" xml:id="sj007_com_Mao">《兔罝》，后妃之化也。《關雎》之化行，則莫不好德，賢人眾多也。</ab>
        <ab resp="#LDM" xml:id="sj007_com_LDM">菟罝，菟又作兔，他故反；罝音子斜反，《說文》子餘反。好，呼報反。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj007 #sj007_com_Mao">
          <head>［疏］「《兔罝》三章，章四句」至「眾多」。正義曰：</head>
          <ab>作《兔罝》詩者，言后妃之化也。言由后妃《關雎》之化行，則天下之人莫不好德，是故賢人眾多。由賢人多，故兔罝之人猶能恭敬，是後妃之化行也。經三章皆言賢人眾多之事也。經直陳兔罝之人賢，而雲多者，箋云：罝兔之人，鄙賤之事，猶能恭敬，則是賢人眾多。是舉微以見著也。《桃夭》言后妃之所致，此言后妃之化，《芣苡》言后妃之美。此三章所美如一，而設文不同者，以《桃夭》承《螽斯》之後，《螽斯》以前皆后妃身事，《桃夭》則論天下昏姻得時，為自近及遠之辭，故云所致也。此《兔罝》又承其後，已在致限，故變言之化，明后妃化之使然也。《芣苡》以後妃事終，故總言之美。其實三者義通，皆是化美所以致也。又上言不妒忌，此言《關雎》之化行，不同者，以《桃夭》說昏姻男女，故言不妒忌，此說賢人眾多，以《關雎》求賢之事，故言《關雎》之化行。《芣苡》則婦人樂有子，故云和平。序者隨義立文，其實總上五篇致此三篇。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj007">
      <lg type="stanza" xml:id="sj007s01">
        <lg type="couplet" xml:id="sj007s01e1">
          <l xml:id="sj007s01l1">肅肅兔罝、</l>
          <l xml:id="sj007s01l2">椓之丁丁。</l>
        </lg>
        <lg type="couplet" xml:id="sj007s01e2">
          <l xml:id="sj007s01l3">赳赳武夫、</l>
          <l xml:id="sj007s01l4">公侯干城。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj007s02">
        <lg type="couplet" xml:id="sj007s02e1">
          <l xml:id="sj007s02l1">肅肅兔罝、</l>
          <l xml:id="sj007s02l2">施于中逵。</l>
        </lg>
        <lg type="couplet" xml:id="sj007s02e2">
          <l xml:id="sj007s02l3">赳赳武夫、</l>
          <l xml:id="sj007s02l4">公侯好仇。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj007s03">
        <lg type="couplet" xml:id="sj007s03e1">
          <l xml:id="sj007s03l1">肅肅兔罝、</l>
          <l xml:id="sj007s03l2">施于中林。</l>
        </lg>
        <lg type="couplet" xml:id="sj007s03e2">
          <l xml:id="sj007s03l3">赳赳武夫、</l>
          <l xml:id="sj007s03l4">公侯腹心。</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj007s01" type="commentaries">
      <div source="#sj007s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj007s01e1_com_Mao">
          <ab>肅肅，敬也。兔罝，兔罟也。丁丁，椓杙聲也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj007s01e1_com_ZX">
          <head>箋云：</head>
          <ab>罝兔之人，鄙賤之事，猶能恭敬，則是賢者眾多也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj007s01e1_com_LDM">
          <ab>椓，陟角反。丁，陟耕反。罟音古，罔也。杙，本又作弋，羊職反，郭羊北反。《爾雅》云「枳謂之杙」，李巡云：「橛也。」枳音特。橛音其月反。</ab>
        </div>
      </div>
      <div source="#sj007s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj007s01e2_com_Mao">
          <ab>赳赳，武貌。乾，捍也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj007s01e2_com_ZX">
          <head>箋云：</head>
          <ab>乾也，城也，皆以禦難也。此罝兔之人，賢者也，有武力，可任為將帥之德，諸侯可任以國守，捍城其民，折沖禦難於未然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj007s01e2_com_LDM">
          <ab>赳，居黝反，《爾雅》云：「勇也。」幹如字，孫炎注云：「乾，楯，所以自蔽捍也。」舊戶旦反，沈音干。捍，戶旦反。御，魚呂反。難，乃旦反，下同。任音壬。將，子匠反。帥，色類反，沈所愧反。「可任」，而鴆反，後不音者放此。守，手又反。折，之役反。沖，昌容反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj007s01e1_com_Mao">
          <head>傳「肅肅」至「杙聲」。正義曰：</head>
          <ab>「肅肅，敬也」，《釋訓》文。此美其賢人眾多，故為敬。《小星》云「肅肅宵征」，故傳曰：「肅肅，疾貌。」《鴇羽》、《鴻雁》說鳥飛，文連其羽，故傳曰：「肅肅，羽聲也。」《黍苗》說宮室，箋云：「肅肅，嚴正之貌。」各隨文勢也。《釋器》云：「兔罟謂之罝。」李巡曰：「兔自作徑路，張罝捕之也。」《釋宮》云：「枳謂之杙。」李巡云：「杙謂鬘也。」此「丁丁」連「椓之」，故知椓杙聲，故《伐木》傳亦云：「丁丁，伐木聲。」</ab>
        </div>
        <div source="#sj007s01e2_com_Mao">
          <head>傳「乾，捍也」。正義曰：</head>
          <ab>《釋言》文。孫炎曰：「乾，盾，自蔽捍也。」下傳曰：「可以制斷，公侯之腹心。」是公侯以為腹心。則好仇者，公侯自以為好匹；干城者，公侯自以為捍城。言以武夫自固，為捍蔽如盾，為防守如城然。</ab>
        </div>
        <div source="#sj007s01e2_com_ZX">
          <head>箋「乾也」至「未然」。正義曰：</head>
          <ab>箋以此武夫為捍城其民，易傳者以其赳赳武夫，論有武任，明為民捍城，可以禦難也。言未然者，謂未有來侵者，來則折其沖，御其難也。若使和好，則此武夫亦能和好之，故二章云公侯好仇。</ab>
        </div>
      </div>
    </div>
    <div source="#sj007s02" type="commentaries">
      <div source="#sj007s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj007s02e1_com_Mao">
          <ab>逵，九達之道。</ab>
        </div>
        <div resp="#LDM" xml:id="sj007s02e1_com_LDM">
          <ab>施，如字。逵，求龜反。杜預注《春秋》云：「塗方九軌。」</ab>
        </div>
      </div>
    </div>
    <div source="#sj007s03" type="commentaries">
      <div source="#sj007s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj007s03e1_com_Mao">
          <ab>中林，林中。</ab>
        </div>
        <div resp="#LDM" xml:id="sj007s03e1_com_LDM">
          <ab>施如字，沈以豉反。</ab>
        </div>
      </div>
      <div source="#sj007s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj007s03e2_com_Mao">
          <ab>可以制斷，公侯之腹心。</ab>
        </div>
        <div resp="#ZX" xml:id="sj007s03e2_com_ZX">
          <head>箋云：</head>
          <ab>此罝兔之人，於行攻伐，可用為策謀之臣，使之慮無，亦言賢也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj007s03e2_com_LDM">
          <ab>斷，丁亂反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj007s03e2_com_Mao">
          <head>傳「可以」至「腹心」。正義曰：</head>
          <ab>解武夫可為腹心之意。由能制斷，公侯之腹心；以能制治，己之腹心；臣之倚用，如己腹心。</ab>
        </div>
        <div source="#sj007s03e2_com_ZX">
          <head>箋「此罝」至「言賢」。正義曰：</head>
          <ab>箋以首章為御難，謂難未至而預禦之。二章為和好怨耦，謂己被侵伐，使和好之也。皆是用兵之事，故知此腹心者，謂行攻伐，又可以為策謀之臣，使之慮無也。慮無者，宣十二年《左傳》文也，謀慮不意之事也。今所無，不意有此，即令謀之，出其奇策也。言用策謀，明自往攻伐，非和好兩軍，與二章異也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《兔罝》三章，章四句。</ab>
  </back>
</text>
</TEI>
