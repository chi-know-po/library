# MetadataToRepository: Explanations

This folder contains:
- One notebook to generate a csv files that stores all necessary metadata for the Nakala Repository
- One notebook to send a set of files along with rich, multilingual metadata to the Nakala Repository.

Note that:
- the csv automatically generated should be revised manually for titles to correspond to each subset
- the csv is also usefull for TEI edition.