import re
from bs4 import BeautifulSoup
from loguru import logger
from pathlib import Path

def add_predefined_collection(html_path: Path, collection_name: str, collection_id: str):
    """
    Adds a predefined collection to the JavaScript collections array in an HTML form.

    Args:
    html_path (Path): The path to the HTML file.
    collection_name (str): The name of the new collection to add.
    collection_id (str): The identifier of the new collection to add.
    """
    if not collection_id or not collection_name:
        logger.error("The collection name and collection id must be provided.\n")
        return
    # Load the HTML file
    with open(html_path, "r", encoding="utf-8") as file:
        soup = BeautifulSoup(file, "html.parser")

    # Find the script tag that contains the JavaScript array
    script_tag = soup.find("script", string=re.compile(r'const predefinedCollections = \['))
    if script_tag is None:
        logger.error("Could not find the JavaScript predefinedCollections array in the HTML file.\n")
        return

    # Extract the JavaScript code and locate the predefinedCollections array
    script_content = script_tag.string
    match = re.search(r'const predefinedCollections = \[(.*?)\];', script_content, re.DOTALL)
    if not match:
        logger.error("Could not find the predefinedCollections array in the script content.\n")
        return

    # Extract the current contents of the predefined collections array
    collections_content = match.group(1).strip()

    # Check if the collection_name or collection_id already exists
    existing_entries = re.findall(r"\{ name: '(.*?)', id: '(.*?)' \}", collections_content)
    for existing_name, existing_id in existing_entries:
        if existing_name == collection_name:
            logger.warning(f'The collection "{collection_name}" (with associated ID "{existing_id}") already exists as a predefined collection.\n')
            return
        if existing_id == collection_id:
            logger.warning(f'The collection id "{collection_id}" (with associated name "{existing_name}") already exists as a predefined collection.\n')
            return

    # Append the new collection to the predefined collections array
    new_collection = f"{{ name: '{collection_name}', id: '{collection_id}' }}"
    if collections_content:
        updated_collections_content = collections_content + ",\n        " + new_collection
    else:
        updated_collections_content = new_collection

    # Replace the old array with the updated one in the script content
    updated_script_content = script_content.replace(
        match.group(1),
        updated_collections_content
    )
    script_tag.string.replace_with(updated_script_content)

    # Pretty print the updated HTML
    pretty_html = soup.prettify()

    # Save the updated HTML file
    with open(html_path, "w", encoding="utf-8") as file:
        file.write(pretty_html)

    logger.info(f'The collection "{collection_name}" (collection_id) has been successfully added as a predefined collection.\n')
