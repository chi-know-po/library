import csv
import json
import requests
import sys
import time
import yaml
from pathlib import Path
from typing import List, Dict, Tuple
from loguru import logger

baseTypeUri = "http://www.w3.org/2001/XMLSchema"
nakalaTermsURL = "http://nakala.fr/terms"
purlTermsURL = "http://purl.org/dc/terms"

logger.remove()
logger.add(sys.stderr, colorize=True)

# Function to get the API credentials

def get_api_credentials(config_yaml: Path, prod = False) -> Tuple[str, str]:
    """
    Use the config for NAKALA API and return the API key and the API URL.

    Args:
        config_yaml (Path): Path to the configuration file.
        prod (bool): Whether to use the production API. Default is False (uses the test API).

    Returns:
        Tuple[str, str]: The API key and the API URL.
    """
    if prod:
        nakalaEnv = "prod"
    else:
        nakalaEnv = "test"
    # get the API URL and key from the env YAML file
    with open(config_yaml, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
        apiUrl = cfg["Nakala"][nakalaEnv]["apiUrl"]
        apiKey = cfg["Nakala"][nakalaEnv]["apiKey"]
        if not apiKey or apiKey == "your-prod-api-key-here":
            raise ValueError(f"No API key found for Nakala {nakalaEnv} environment")
    logger.info(f"API credentials found for Nakala {nakalaEnv} environment")
    return apiKey, apiUrl


# Collection functions: search, creation and deletion 

def look_for_collection(string: str, apiUrl: str) -> str:
    """
    Look for a collection in the API and return the identifier.

    Args:
        string (str): The string to look for.
    Returns:
        str: The identifier of the collection. None if no collection is found.
    """
    getCollectionInfo_url = f"{apiUrl}/search?q={string}&order=relevance&page=1&size=25"

    verb = "GET"

    getDataResponse = requests.request(verb, getCollectionInfo_url)
    response = getDataResponse.json()

    if response["totalResults"] == 0:
        logger.error(f"No collection found for {string}")
        return None
    collection_mapping_exact = [data for data in response["datas"] if "/collection/" in data["uri"] and any(meta["propertyUri"].endswith("/terms#title") and meta["value"] == string for meta in data["metas"])]
    if len(collection_mapping_exact) == 1:
        collection = collection_mapping_exact[0]
        identifier = collection["identifier"]
        logger.info(f"One collection found for {string} with identifier {identifier}")    
        return identifier
    elif len(collection_mapping_exact) > 1:
        collection = collection_mapping_exact[0]
        identifier = collection["identifier"]
        logger.warning(f"Multiple collections found for {string}, the first one will be used (identifier: {identifier})")
        return identifier
    else:
        # search for collections even if the string is not the title
        collection_mapping = [data for data in response["datas"] if "/collection/" in data["uri"] and any(meta["propertyUri"].endswith("/terms#title") for meta in data["metas"])]
        if len(collection_mapping) == 0:
            logger.error(f"No collection found for {string}")
            return None
        elif len(collection_mapping) == 1:
            collection = collection_mapping[0]
            identifier = collection["identifier"]
            # get the title of the collection
            title = [meta["value"] for meta in collection["metas"] if meta["propertyUri"].endswith("/terms#title")][0]
            logger.warning(f"One collection found but the title is different: {title} (identifier: {identifier})")
            return identifier
        else:
            collection = collection_mapping[0]
            identifier = collection["identifier"]
            title = [meta["value"] for meta in collection["metas"] if meta["propertyUri"].endswith("/terms#title")][0]
            logger.warning(f"Multiple collections found for {string}, the first one will be used (identifier: {identifier})")
            logger.warning(f"The title of the collection is: {title}")
            return collection_mapping[0]["identifier"]
        
def create_collection(apiUrl: str, apiKey: str, new_collection_name: str, lang = "en", public = True) -> str:
    """
    Create a new collection in the Nakala API and return its identifier

    Args:
        apiUrl (str): URL of the Nakala API
        apiKey (str): API key to access the Nakala API
        new_collection_name (str): Name of the new collection
        lang (str): Language of the collection. Defaults to "en".
        public (bool): Visibility of the collection. If False, it will be private. Defaults to True.

    Returns:
        str: Identifier of the new collection. None if the creation failed.
    """
    if public:
        status = "public"
    else:
        status = "private"

    headers = {
        "X-API-KEY": apiKey,
        "Content-Type": "application/json"
        }

    base_uri = apiUrl.replace("https://apitest.", "http://")
    content = {
        "status": status,
        "metas" :[
            {
                "propertyUri": f"{base_uri}/terms#title",
                "value": new_collection_name,
                    "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                "lang": lang
            }
            ]
    }
    
    url = f"{apiUrl}/collections"
    verb = "POST"

    createCollectionResponse = requests.request(verb, url, headers=headers, data=json.dumps(content))
    parsedcreateCollectionResponse = json.loads(createCollectionResponse.text)
    if createCollectionResponse.status_code == 201:
        # Retrieve collection ID from API returned info
        identifier = parsedcreateCollectionResponse["payload"]["id"]
        metas=content["metas"]
        metas=metas[0]
        newCollectionTitle=metas["value"]
        logger.success(f"{newCollectionTitle} was sucessfully created with id {identifier}")
        return identifier
    else:
        logger.error("Collection was not created.")
        logger.info(f"Status code: {createCollectionResponse.status_code}")
        logger.info(f"Response: {parsedcreateCollectionResponse.get("message")}")
        return None

def remove_collection_from_nakala(identifier: str, apiUrl: str, apiKey: str):
    """
    Remove a collection from Nakala chosen environment

    Args:
        identifier (str): the identifier of the collection to remove
        apiUrl (str): the Nakala API URL
        apiKey (str): the API key to access the Nakala API
    """
    url = f"{apiUrl}/collections/{identifier}"
    verb = "DELETE"

    headers = {
        "X-API-KEY": apiKey,
        "Content-Type": "application/json"
    }

    deleteCollectionResponse = requests.request(verb, url, headers=headers)
    if deleteCollectionResponse.status_code in [200, 204]:
        logger.success(f"Collection (id: {identifier}) was successfully deleted.")
    else:
        logger.error(f"Error while deleting collection (id: {identifier}).")
        logger.info(deleteCollectionResponse.text)

def remove_collections_from_nakala(list_of_identifiers: List[str], apiUrl: str, apiKey: str):
    """
    Remove a list of collections from Nakala chosen environment

    Args:
        list_of_identifiers (List[str]): the list of collection identifiers to remove
        apiUrl (str): the Nakala API URL
        apiKey (str): the API key to access the Nakala API
    """
    for identifier in list_of_identifiers:
        remove_collection_from_nakala(identifier, apiUrl, apiKey)

# Upload a dataset to NAKALA repository

def nakala_uploader(apiKey: str,
                    apiUrl: str,
                    dataset: List[Tuple],
                    outputFile: Path,
                    dataFolder: Path,
                    language1: Dict[str, str],
                    language2: Dict[str, str]):
    """
    Function to upload data to NAKALA repository

    Args:
        apiKey (str): API key to access NAKALA repository
        apiUrl (str): URL of NAKALA repository
        dataset (List[Tuple]): List of tuples with data to upload
        outputFile (Path): Path to output file
        dataFolder (Path): Path to data folder
        language1 (Dict[str, str]): Main language (Nakala API convention) of metadata (e.g. {"id": "en", "label": "anglais"} for English)
        language2 (Dict[str, str]): Second language of metadata (e.g. {"id": "zh", "label": "langues chinoises"} for Chinese)
    """
    # Open output file
    output = open(outputFile, "w", newline="")
    outputWriter = csv.writer(output)

    # cell ABC
    for num, data in enumerate(dataset):
        filenames = data[0].split("|") # make sure to use pipes to separate filenames in first cell 
        embargo = data[1]
        status = data[2]
        datatype = data[3]
        title2 = data[4]
        title1= data[5] # choose iso identifier to identify second language used for metadata
        authors = list(filter(None,data[6].split("|"))) # includes a filter not to keep empty cells(mandatory information)
        date = data[7]
        license = data[8]
        descriptionMain = data[9]
        descriptionSec = data[10]
        keywordslanguage1 = list(filter(None,data[11].split("|")))
        keywordslanguage2 = list(filter(None,data[12].split("|")))
        keywordsURI = list(filter(None,data[13].split("|"))) # try to use one type of standard for keywords
        language = data[14]
        creators = list(filter(None,data[15].split("|"))) # It might be possible to use URIs to point towards official Ids (IdRef, ORCID, etc.)
        contributors = list(filter(None,data[16].split("|"))) # We use NISO standard called Credit: https://credit.niso.org/
        created = data[17] # choose a standard for the project
        dctermsAvailable = data[18]
        modified = data[19]
        dctermsPublisher = data[20]
        formats = list(filter(None,data[21].split("|")))
        conformTo = list(filter(None,data[22].split("|")))
        version = data[23]
    # These pieces of info need to b added
        nakalaCollections = list(filter(None,data[24].split("|")))
    #    datarights = list(filter(None,data[25].split("|")))
        
        outputData = ["","",title2,"",""]; # all variables to be attached to the deposit are stored in this list    
        logger.info("THE FOLLOWING SET OF DATA WAS CREATED : n°" + str(num) + "  " + title2)
        
        # Send files through API
        files = [] # We create a list to compile information returned in JSON through API.
        outputFiles = [] # We create a list to store the information that will be added to the output file used to make sure the entire transfert went well.
        for filename in filenames:
            logger.info("Uploading files " + filename + "...")
            goToNextData = False
            payload={}
            postfiles=[("file", (filename, open(Path(dataFolder / filename), "rb")))]
            headers = {"X-API-KEY": apiKey }
            response = requests.request("POST", apiUrl + "/datas/uploads", headers=headers, data=payload, files=postfiles)
            # If upload went well
            if (201 == response.status_code):
                # add embargo date whether initially present or not
                file = json.loads(response.text)
                file["embargoed"] = embargo or time.strftime("%Y-%m-%d")
                files.append(file)
                outputFiles.append(filename + ";" + file["sha1"])
            else:
                # If upload didn"t go well, output fille will include "ERROR" info
                outputFiles.append(filename)
                outputData[1] = "|".join(outputFiles)
                outputData[3] = "ERROR"
                outputData[4] = response.text
                outputWriter.writerow(outputData)
                logger.warning("Some files could not be uploaded, process resumes...")
                goToNextData = True
                break
        if goToNextData: continue
        outputData[1] = "|".join(outputFiles)
        
        # Reconstruct metadata
        metas = [] # list to store all metadata in specific format
        
        # Mandatory metadata (datatype)
        metaType = {
            "value": datatype, # column "datatype" (one value per deposit)
            "typeUri": f"{baseTypeUri}#anyURI", # standard format of value: URI
            "propertyUri": f"{nakalaTermsURL}#type"
        }
        metas.append(metaType) # metadata added to "metas" dataframe

            # Mandatory metadata (title)
            # Beware:split added to attach proper language
        metatitle1 = {
            "value": title2, # column "nakala:title (fr)" (one value per deposit)
            "lang": language2, # title language (cf. ISO-639-1 or ISO-639-3 for less common languages)
    ###---------------------------------Change language if needed ###
            "typeUri": f"{baseTypeUri}#string", # standard format: string
            "propertyUri": f"{nakalaTermsURL}#title"
        }
        metas.append(metatitle1)
        # add title in Chinese if present, for example
        if title1:
            metatitle2 = {
                "value": title1, # column "nakala:title (zh)" (one value per deposit)
                "lang": language1, # title language (cf. ISO-639-1 or ISO-639-3 for less common languages)
    ###---------------------------------Change language if needed ###
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{nakalaTermsURL}#title"
            }
            metas.append(metatitle2)

            # Mandatory metadata (author) -- mandatory to publish deposit        
        if not authors: # if cell empty, adds value "anonyme" for nakala:creator
            metaAuthor = {
            "value": None,
            "propertyUri": f"{nakalaTermsURL}#creator"
            }
            metas.append(metaAuthor)
        else:    
            for author in authors: # column "nakala:creator" (multiple values per deposit possible)
                surnameGivennameORCID = author.split(";")
                metaAuthor = {
                    "value": #author,
                    { # standard format: string for each author, authors separated by "|"
                        "givenname": surnameGivennameORCID[1],
                        "surname": surnameGivennameORCID[0]
                    },
                    "propertyUri": f"{nakalaTermsURL}#creator"
                }
                # ORCID added if present
                if len(surnameGivennameORCID) == 3:
                    metaAuthor["value"]["orcid"] = surnameGivennameORCID[2]
                metas.append(metaAuthor)

            # Mandatory metadata (creation date) -- mandatory to publish deposit
        if not date: # if cell empty, adds value "inconnue" for "nakala:created"
            metaDate = {
                "value": None,
                "propertyUri": f"{nakalaTermsURL}#created"
            }
        else:
            metaDate = {
                "value": date, # column "nakala:created" (one value per deposit)
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{nakalaTermsURL}#created"
            }
        metas.append(metaDate)
        
            # Mandatory metadata (license)
        metaLicense = {
            "value": license, # column "nakala:license" (one value per deposit)
            "typeUri": f"{baseTypeUri}#string",
            "propertyUri": f"{nakalaTermsURL}#license"
        }
        metas.append(metaLicense)
        
        # Optional metadata (description)
        metaDescriptionMain = {
            "value": descriptionMain, #column "dcterms:description (en)" (one value per deposit)
            "lang": language2, # (cf. ISO-639-1 or ISO-639-3 for less common languages)
    ###---------------------------------Change language if needed ###
            "typeUri": f"{baseTypeUri}#string",
            "propertyUri": f"{purlTermsURL}/description"
        }
        metas.append(metaDescriptionMain)
        # add description in another language if present
        if descriptionSec:
            metaDescriptionSec = {
                "value": descriptionSec,
                "lang": language1, # (cf. ISO-639-1 or ISO-639-3 for less common languages)
    ###---------------------------------Change language if needed ###
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/description"
            }
            metas.append(metaDescriptionSec)
        
        # Optional metadata (keywords)
        for keywordlanguage1 in keywordslanguage1: # column "dcterms:subject (zh)"
            metaKeywordlanguage1 = {
                "value": keywordlanguage1, # on insère ici la valeur d"un mot-clé
                "lang": language1, # (cf. ISO-639-1 or ISO-639-3 for less common languages)
    ###---------------------------------Change language if needed ###
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/subject"
            }
            metas.append(metaKeywordlanguage1)
        
        # Optional metadata (keywords) if present in two languages
        for keywordlanguage2 in keywordslanguage2: # column "dcterms:subject (en)"
            metaKeywordlanguage2 = {
                "value": keywordlanguage2,
                "lang": language2, # (cf. ISO-639-1 or ISO-639-3 for less common languages)
    ###---------------------------------Change language if needed ###
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/subject"
            }
            metas.append(metaKeywordlanguage2)

            # Optional metadata (keywords)-- URIs
        for keywordURI in keywordsURI: # column "dcterms:subject (URI)"
            metaKeywordURI = {
                "value": keywordURI,
                "typeUri": f"{baseTypeUri}#anyURI", # standard value: URI
                "propertyUri": f"{purlTermsURL}/subject"
            }
            metas.append(metaKeywordURI)
        
            # Optional metadata (dcterms:language) -- which language is used in file
        if language:
            metaLanguage = {
                "value": language,
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/language"
            }
            metas.append(metaLanguage)
        
        # Optional metadata (dcterms:creator) -- for authors for which no separation between family name and surname
        if creators:
            for creator in creators:
                metaCreator = {
                    "value": creator,
                    "typeUri": f"{baseTypeUri}#string",
                    "propertyUri": f"{purlTermsURL}/creator"
                    
                }
                metas.append(metaCreator)
            
        # Optional metadata (dcterms:contributor) -- for other contributors, no standard form. For our project, we use NISO Credit vocabulary to describe function, attached to person, and if possible IDs.
        if contributors:
            for contributor in contributors:
                metaContributor = {
                    "value": contributor,
                    "typeUri": f"{baseTypeUri}#string",
                    "propertyUri": f"{purlTermsURL}/contributor"    
                }
                metas.append(metaContributor)
        
        # Optional metadata -- for dates not expressed as: AAAA, AAAA-MM, AAAA-MM-JJ
        if created:
            metaCreated = {
                "value": created, # one value per deposit
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/created"
            }
            metas.append(metaCreated)
                            
        #  Optional metadata (dcterms:available) -- dates standard format: AAAA, AAAA-MM, AAAA-MM-JJ
        if dctermsAvailable:
            metaAvailable = {
                "value": dctermsAvailable, # column "dcterms:created" (one value per deposit)
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/available"
            }
            metas.append(metaAvailable)   
            
        # Optional metadata -- indicates modification, useful if deposit is modified (new version) 
        if modified:
            metaModified ={
                "value": modified, # one date per deposit
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/modified"
            }
            metas.append(metaModified)
        
        # Optional metadata ("dcterms:publisher")
        if dctermsPublisher:
            metaDctermsPublisher = {
                "value": dctermsPublisher, # column "dcterms:publisher" (one value per deposit)
                "lang": language2,
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/publisher"
            }
            metas.append(metaDctermsPublisher)
        
        # Optional metadata -- formats
        for formatype in formats: #column "dcterms:format (URI)"
            metaFormat = {
                "value": formatype,
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/format"
            }
            metas.append(metaFormat)

        # Optional metadata -- standards
        for standard in conformTo: # column "dcterms:conformsTo (URI)"
            metaStandard = {
                "value": standard,
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/conformsTo"
            }
            metas.append(metaStandard)
        
        # Optional metadata -- version number
        if version:
            metaVersion = {
                "value": version,
                "typeUri": f"{baseTypeUri}#string",
                "propertyUri": f"{purlTermsURL}/hasVersion"
            }
            metas.append(metaVersion)
        
        # Reconstruct rights on deposit
    #    rights = []
    #    for dataright in datarights:
    #        datarightSplit = dataright.split(";") # format for each value: id and role separated by ";"
    #        if len(datarightSplit) == 2:
    #            right = {
    #                "id": datarightSplit[0],
    #                "role": datarightSplit[1]
    #            }
    #            rights.append(right)
        
        # Reconstruct collections
        collectionsIds = []
        for nakalaCollection in nakalaCollections:
            collectionsIds.append(nakalaCollection)
        
        # Send data to NAKALA repository
        postdata = {
            "status" : status,
            "files" : files,
            "metas" : metas,
    #        "rights": rights,
            "collectionsIds": collectionsIds
        }
        content = json.dumps(postdata)
        headers = {
        "Content-Type": "application/json",
        "X-API-KEY": apiKey,
        }
        response = requests.request("POST", apiUrl + "/datas", headers=headers, data=content)
        logger.info(response.status_code)
        if ( 201 == response.status_code):
            parsed = json.loads(response.text)
            logger.success("Data deposit n° " + str(num) + " properly created with id: " + parsed["payload"]["id"] + "\n")
            outputData[0] = parsed["payload"]["id"]
            outputData[3] = "OK"
            outputData[4] = response.text
        else:
            logger.exception("Something went wrong!")
            outputData[3] = "ERROR"
            outputData[4] = response.text
        outputWriter.writerow(outputData)

    # Close output file
    output.close()
