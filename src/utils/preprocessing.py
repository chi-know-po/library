import re

def remove_punctuation(text: str) -> str:
    """
    Remove all punctuation from the given text.

    Args:
        text: The input string from which punctuation should be removed.
    Returns:
        str: The text with all punctuation characters removed.
    """
    if isinstance(text, str):
        return re.sub(r'[^\w\s]', '', text)
    return text