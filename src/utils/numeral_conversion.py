"""
Module for converting Arabic digits to Chinese numbers and vice versa.
"""
from typing import List

# Mapping Arabic digits with Chinese numbers (basic units)
NUM_CN = {
    0: '〇',
    1: '一',
    2: '二',
    3: '三',
    4: '四',
    5: '五',
    6: '六',
    7: '七',
    8: '八',
    9: '九'
}

# Mapping Chinese characters to Arabic numbers
CN_NUM = {
    '〇': 0,
    '一': 1,
    '二': 2,
    '三': 3,
    '四': 4,
    '五': 5,
    '六': 6,
    '七': 7,
    '八': 8,
    '九': 9,

    '零': 0,
    '壹': 1,
    '贰': 2,
    '叁': 3,
    '肆': 4,
    '伍': 5,
    '陆': 6,
    '柒': 7,
    '捌': 8,
    '玖': 9,

    '貮': 2,
    '两': 2,
}

# Mapping Chinese units to their values
CN_UNIT = {
    '十': 10,
    '拾': 10,
    '百': 100,
    '佰': 100,
    '千': 1000,
    '仟': 1000,
    '万': 10000,
    '萬': 10000,
    '亿': 100000000,
    '億': 100000000,
    '兆': 1000000000000,
}

def integer_to_chinese_num(num: int) -> str:
    """
    Converts an integer to a Chinese number.
    
    Args:
        num: The integer to convert.
        
    Returns:
        str: The Chinese number as a string.
    """
    if num < 10:
        return NUM_CN[num]
    elif num < 20:  # dealing with numbers from 10 to 19
        return '十' + NUM_CN[num % 10].rstrip('〇')
    elif num < 100:  # from 20 to 99
        return NUM_CN[num // 10] + '十' + NUM_CN[num % 10].rstrip('〇')
    elif num < 1000:  # from 100 to 999
        return NUM_CN[num // 100] + '百' + integer_to_chinese_num(num % 100).lstrip('一').rstrip('〇')
    elif num < 10000:  # from 1000 to 9999
        return NUM_CN[num // 1000] + '千' + integer_to_chinese_num(num % 1000).lstrip('一').rstrip('〇')
    # numbers above 10000 are not dealt with
    return str(num)

def chinese_num_to_integer(cn: str) -> int:
    """
    Converts a Chinese number to an Arabic number.
    
    Args:
        cn: The Chinese number to convert.
        
    Returns:
        int: The Arabic number.
    """
    lcn = list(cn)
    unit: int = 0
    digits_list = []

    while lcn:
        cndig = lcn.pop()
        if cndig in CN_UNIT:
            unit = CN_UNIT[cndig]
            if unit in [10000, 100000000, 1000000000000]:
                digits_list.append(cndig)
                unit = 1
        else:
            dig = CN_NUM[cndig]
            if unit:
                dig *= unit
                unit = 0
            digits_list.append(dig)

    if unit == 10:  # dealing with numbers from 10 to 19
        digits_list.append(10)

    return _convert_digits_list_to_number(digits_list)

def _convert_digits_list_to_number(digits_list: List[int]) -> int:
    """
    Helper function to convert a list of digits to a number.
    
    Args:
        digits_list: The list of digits and unit markers.
        
    Returns:
        int: The final number.
    """
    ret = 0
    tmp = 0

    while digits_list:
        x = digits_list.pop()

        if x == '万':
            tmp *= 10000
            ret += tmp
            tmp = 0
        elif x == '亿':
            tmp *= 100000000
            ret += tmp
            tmp = 0
        elif x == '兆':
            tmp *= 1000000000000
            ret += tmp
            tmp = 0
        else:
            tmp += x

    ret += tmp
    return ret


if __name__ == '__main__':
    # testing different conversions
    test_arabic_numbers = [100, # 一百
                           101, # 一百〇一
                           110, # 一百一十
                           111  # 一百一十一
                           ]
    for num in test_arabic_numbers:
        print(f"{num} -> {integer_to_chinese_num(num)}")
    test_dig = [u'九', # 9
                u'十二', # 12
                u'一百二十三', # 123
                u'一千二百零三', # 1203
                u'一万一千一百零一', # 11101
                u'十万零三千六百零九', # 103609
                u'一百二十三万四千五百六十七', # 1234567
                u'一千一百二十三万四千五百六十七', # 11234567
                u'一亿一千一百二十三万四千五百六十七', # 111234567
                u'一百零二亿五千零一万零一千零三十八', # 102501001038
                u'一千一百一十一亿一千一百二十三万四千五百六十七', # 1111111234567
                u'一兆一千一百一十一亿一千一百二十三万四千五百六十七', # 11111111234567
    ]
    for cn in test_dig:
        print(f"{cn} -> {chinese_num_to_integer(cn)}")
