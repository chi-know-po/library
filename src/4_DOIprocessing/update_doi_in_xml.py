from loguru import logger
from lxml import etree
from pathlib import Path

def replace_doi_in_file(file: Path, doi: str):
    """
    Replace the DOI in the file by the correct DOI

    Args:
        file (Path): the file to update
        doi (str): the corresponding DOI
    """
    # Use pathlib to read the file
    with file.open("rb") as f:  # Open in binary mode
        content = f.read()

    # Use lxml to parse the content
    try:
        tree = etree.fromstring(content)
    except etree.XMLSyntaxError as e:
        logger.error(f"Error parsing XML from {file}: {e}")
        return

    # Find the DOI in the tree
    doi_node = tree.find(".//idno[@type='DOI']")

    if doi_node is None:
        logger.warning(f"DOI not found in {file}")
        return

    # Replace the DOI in the tree
    doi_node.text = doi

    # Add utf-8 declaration
    new_content = (
        '<?xml version="1.0" encoding="utf-8"?>\n' +  
        etree.tostring(tree, pretty_print=True, encoding="unicode")
    )

    # Write the content back to the file
    with file.open("w", encoding="utf-8") as f:
        f.write(new_content)

    logger.info(f"DOI replaced in {file}")
