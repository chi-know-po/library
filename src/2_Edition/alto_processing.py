import re
import numpy as np
import pandas as pd
import sys
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import List, Dict, Set
from loguru import logger
sys.path.append(str(src := Path(__file__).resolve().parents[1])) # add the src directory to sys.path
from utils.numeral_conversion import chinese_num_to_integer

namespaces = {'alto': 'http://www.loc.gov/standards/alto/ns-v4#'}

def parse_xml_files(alto_folder_path: Path) -> int:
    """
    Parse all XML files in the specified folder.

    Args:
        alto_folder_path: Path to the folder containing ALTO XML files.

    Returns:
        int: Number of successfully parsed XML files.
    """
    xml_files = alto_folder_path.glob("*.xml")
    successful_count = 0

    for file_path in xml_files:
        try:
            ET.parse(file_path).getroot()
            successful_count += 1
        except ET.ParseError as e:
            logger.warning(f'Failed to parse {file_path}: {e}')

    total_files = len(list(alto_folder_path.glob("*.xml")))
    logger.info(f'Successfully parsed {successful_count} files out of {total_files}')
    return successful_count

def check_row(row: pd.Series) -> bool:
    """
    Check if all columns except 'Page_Number' and 'filename' are null
    
    Args:
        row: a row of the DataFrame
    
    Returns:
        bool: True if all columns except 'Page_Number' and 'filename' are null, otherwise False.
    """
    other_columns = row.drop(['Page_Number', 'filename'])
    return other_columns.isnull().all()

def assign_labels(group: pd.DataFrame) -> pd.Series:
    """
    Assign letter labels to unique filenames within each Page_Number group.
    
    Args:
        group: DataFrame group
    
    Returns:
        pd.Series: A series with letter labels assigned
    """
    unique_filenames = group['filename'].drop_duplicates().tolist()
    filename_to_label = {filename: chr(65 + i) for i, filename in enumerate(unique_filenames)}
    return group['filename'].map(filename_to_label)

def update_wrongful_title_in_row(row: pd.Series, additional_expressions: List[str] = []) -> pd.Series:
    """
    Update the 'Title' and 'Title_remark' columns for a row if the title matches any wrongful patterns.
    
    Args:
        row: a row of the DataFrame
        additional_expressions: Additional regex patterns to identify wrongful titles
    Returns:
        pd.Series: Updated row with wrongful titles moved to 'Title_remark'
    """
    wrongful_patterns: Set[str] = {
        "^目錄$",  # "Table of Contents" in Chinese
        "^卷[一二三四五六七八九十百千万上下]+$"  # "Chapter" + number or "superior"/"inferior" ("上"/"下")
    }
    wrongful_patterns.update(additional_expressions)
    
    for pattern in wrongful_patterns:
        if re.match(pattern, str(row['Title'])):
            row['Title_remark'] = row['Title']
            row['Title'] = None
            break

    return row

def process_alto_folder(alto_folder_path: Path, csv_output: Path, additional_expressions: List = []) -> pd.DataFrame:
    """
    Process a folder of ALTO XML files and create a DataFrame with the extracted data.
    
    Args:
        alto_folder_path: Path to the folder containing ALTO XML files
        csv_output: Path to save the resulting CSV file
        additional_expressions: Additional regex patterns to identify wrongful titles
    Returns:
        pd.DataFrame: DataFrame containing the extracted data
    """
    data: List[Dict] = []
    parse_xml_files(alto_folder_path)  # Ensure XML files are parsed before proceeding

    for filename in sorted(alto_folder_path.iterdir()):
        if filename.suffix == '.xml':
            try:
                tree = ET.parse(filename)
                root = tree.getroot()

                id_to_label: Dict[str, str] = {}
                for tag in root.findall('.//alto:OtherTag', namespaces):
                    id_value = tag.get('ID')
                    label_value = tag.get('LABEL')
                    id_to_label[id_value] = label_value

                for tb in root.findall('.//alto:TextBlock', namespaces):
                    block_shapes = tb.findall('./alto:Shape', namespaces)
                    block_points = [shape.find('./alto:Polygon', namespaces).get('POINTS')
                                    for shape in block_shapes
                                    if shape.find('./alto:Polygon', namespaces) is not None]

                    tb_tagrefs = tb.get('TAGREFS', '').split()
                    tb_labels = [id_to_label.get(tb_tagref) for tb_tagref in tb_tagrefs if tb_tagref in id_to_label]

                    for tl in tb.findall('.//alto:TextLine', namespaces):
                        strings = tl.findall('.//alto:String', namespaces)
                        line_text = ' '.join([string.get('CONTENT') for string in strings if string.get('CONTENT') is not None])

                        tagrefs = tl.get('TAGREFS', '').split()
                        labels = [id_to_label.get(tagref) for tagref in tagrefs if tagref in id_to_label]

                        line_shapes = tl.findall('./alto:Shape', namespaces)
                        line_points = [shape.find('./alto:Polygon', namespaces).get('POINTS')
                                       for shape in line_shapes
                                       if shape.find('./alto:Polygon', namespaces) is not None]

                        data.append({
                            'filename': filename.name,
                            'line_text': line_text,
                            'line_labels': labels,
                            'line_points': '; '.join(line_points),
                            'block_labels': tb_labels,
                            'block_points': '; '.join(block_points),
                        })

            except ET.ParseError as e:
                logger.error(f"Error parsing {filename}: {e}")
            except Exception as e:
                logger.error(f"An error occurred with {filename}: {e}")

    df = pd.DataFrame(data)

    all_labels = set()
    for item in data:
        for label in item['line_labels']:
            if label:
                all_labels.add(label)

    for label in all_labels:
        df[label] = None

    for index, row in df.iterrows():
        for label in row['line_labels']:
            if label:
                df.at[index, label] = row['line_text']

    df['Page_Number'] = df.groupby('filename')['Page_Number'].transform(lambda x: ' '.join(x.dropna().astype(str)))

    df['Page_label'] = df.groupby('Page_Number', group_keys=False).apply(assign_labels)
    df['Page_Type'] = df['Page_Number'].str.split(n=1, expand=True)[0]
    df.loc[df['Page_Type'] == df['Page_Number'], 'Page_Type'] = None
    df['Page_Type'] = df['Page_Type'].bfill()
    df['Page_Number'] = df['Page_Number'].str.replace(str(df['Page_Type']), '')
    df['Page_Number'] = df['Page_Number'].str.replace(' ', '')

    df.loc[df['Page_Number'] == '', 'Page_Number'] = None
    df['Page_Number'] = df['Page_Number'].ffill()

    df['Title_remark'] = None
    df['Back_title'] = None
    df['Back'] = None

    df = df.apply(lambda row: update_wrongful_title_in_row(row, additional_expressions), axis=1)

    # If there is no author name in our ALTO files, we add an empty colum "Author_name"
    if 'Author_name' not in df.columns:
        df['Author_name'] = None
    df = df[['Title', 'Title_remark', 'Text', 'Back_title', 'Back', 'Commentary', 'Author_name', 'Page_Type', 'Page_Number', 'Page_label', 'line_labels', 'line_points', 'block_labels', 'block_points', 'filename']]
    df.to_csv(csv_output, index=False)

    return df

def process_dataframe_with_numbered_index(csv_path: str, df_structure: pd.DataFrame, title_main: str) -> pd.DataFrame:
    """
    Process the dataframe by cleaning and transforming it according to the specified rules.

    Args:
        csv_path: Path to the CSV file.
        df_structure: DataFrame containing the structure.
        title_main: Main title to be used for extracting the title suffix

    Returns:
        pd.DataFrame: The processed DataFrame.
    """
    df = pd.read_csv(csv_path)
    df = df.reset_index(drop=True)
    df["line_labels"] = df["line_labels"].str.strip("[]'\"")
    df["block_labels"] = df["block_labels"].str.strip("[]'\"")
    df = df[df["block_labels"] != "Marginalia_Metadata"]
    # create a new Boolean column that checks if "Title" was originally NaN
    df["had_title"] = df["Title"].notna()
    # Forward-fill the "Title" column
    df["Title"] = df["Title"].fillna(method="ffill")


    pattern = r"(.*)(攷)"
    # @todo: check if this is specific to Bo Wu Zhi

    for i in df.index:
        # check if "Title" is not NaN and is string
        if pd.notna(df["Title"][i]) and isinstance(df["Title"][i], str):
            match = re.match(pattern, df["Title"][i])
            if match:
                df.at[i, "Title"] = match.group(1) + "考" # = studied (Bo Wu Zhi specific)
        if df["Title"][i] == "列國": # = countries (Bo Wu Zhi specific)
            df["Title"][i] = "外國"  # = foreign countries

    # Define structure level (i.e., 1-level structure = "Title", 2-level structure = "Volume" and "Title", 3-level structure = "Volume", "Subvolume1" and "Title")
    # count how many columns ending with "number" are in df_structure
    n_levels = sum([col.endswith("number") for col in df_structure.columns]) # Title_number, (Volume_number), (Subvolume1_number), (Subvolume2_number), etc.
    # if n_levels inferior or equal to 2
    if n_levels <= 2:
        n_subvolumes = 0
    else:
        n_subvolumes = n_levels - 2 # 2 levels are already used for Volume and Title

    

    # /!\ Warning: This part is not working for 3 levels+ structure
    # Here, we merge on Title, but:
    # this does not work on 3 levels when we have content in Volume + Subvolume1 + Title
    # However, it works when we have no content in Title however then Structure:Subvolume1 == Checked:Title
    # and not Title == Title
    # The strategy when working with 4+ levels should be different
    # todo: (For 3-level structure) first, Structure:Subvolume1 == Checked:Title when Title is empty
    if n_levels <= 2: # 1 or 2 levels, simple merge
        merged_df_structure = pd.merge(df, df_structure, on="Title", how="left")
        if n_levels == 1:
            # create column "Title_id" that is equal to "Title_number" in df_structure in string format 01, 02, 11, etc.
            merged_df_structure["Title_number"] = f"{merged_df_structure['Title_number']:02d}"
        else:
            # create column "Title_id" that is equal to "Volume_number" + "Title_number" in df_structure in string format 01-01, 01-02, 22-11, etc.
            # Ensure both 'Volume_number' and 'Title_number' columns are in the correct type for formatting
            merged_df_structure["Volume_number"] = merged_df_structure["Volume_number"].astype('Int64').fillna(0)
            merged_df_structure["Title_number"] = merged_df_structure["Title_number"].astype('Int64').fillna(0)

            # Apply string formatting for each row
            merged_df_structure["Title_id"] = merged_df_structure.apply(lambda x: f"{x['Volume_number']:02d}-{x['Title_number']:02d}", axis=1)

    else:
        if n_levels == 3: # 3 levels
            
            # create column "Title_id" that is equal to "Volume_number" + "Subvolume1_number" + "Title_number" in df_structure in string format 01-01-01, 01-01-02, 01-11-01, etc.
            # but if Title_number is 0, then "Title_id" is equal to "Volume_number" + "Subvolume1_number" in df_structure in string format 01-01, 01-02, 01-11, etc.
            df_structure["Volume_number"] = df_structure["Volume_number"].astype('Int64').fillna(0)
            df_structure["Subvolume1_number"] = df_structure["Subvolume1_number"].astype('Int64').fillna(0)
            df_structure["Title_number"] = df_structure["Title_number"].astype('Int64').fillna(0)
            
            df_structure["Title_id"] = df_structure.apply(lambda x: f"{x['Volume_number']:02d}-{x['Subvolume1_number']:02d}-{x['Title_number']:02d}" 
                                                          if x['Title_number'] != 0 else f"{x['Volume_number']:02d}-{x['Subvolume1_number']:02d}", axis=1)

            # first, df_structure:Subvolume1 == df:Title when Title is empty
            # in other words, in new df merged_df_structure, Title is filled with Subvolume1 when Title is empty
            df_structure["Title"] = df_structure["Title"].fillna("")
            df["Title"] = df["Title"].fillna("")
            # in df_structure, if Title is empty but not Subvolume1, fill Title with Subvolume1 and Subvolume1 becomes empty
            df_structure["Title"] = df_structure.apply(lambda x: x["Subvolume1"] if x["Title"] == "" and pd.notna(x["Subvolume1"]) else x["Title"], axis=1)
            df_structure["Subvolume1"] = df_structure.apply(lambda x: None if x["Title"] == x["Subvolume1"] else x["Subvolume1"], axis=1)
            # if df_structure's Title is empty, fill it with Volume
            df_structure["Title"] = df_structure.apply(lambda x: x["Volume"] if x["Title"] == "" else x["Title"], axis=1)

            # Merge on Title
            #
            # If the merge does not work, try with the title_main before the string in df_structure.
            # Example : we could have "齊民要術"+"卷一" in df and just "卷一" in df_structure
            #
            # 1. Verify Title column exists in both DataFrames
            if 'Title' not in df.columns or 'Title' not in df_structure.columns:
                raise ValueError("Column Title does not exist in one or both DataFrames.")

            # 2. Create a new column in df with the part of Title after title_main
            df['Title_suffix'] = df['Title'].str.replace(f'^{title_main}', '', regex=True)

            # 3. Perform the conditional merge
            merged_df_structure = pd.merge(
                df,
                df_structure,
                left_on='Title_suffix',
                right_on='Title',
                how='left',
                suffixes=('', '_structure')  # Keep original Title column from df
            )

            # 4. Fill exact matches on the original Title column
            exact_match_merge = pd.merge(df, df_structure, on='Title', how='left', suffixes=('', '_structure'))
            merged_df_structure.update(exact_match_merge)

            # 5. Verify Title column exists in the merged DataFrame
            if 'Title' not in merged_df_structure.columns:
                raise ValueError("Column Title does not exist in merged_df_structure after merging.")

            # 6. Drop unnecessary column
            merged_df_structure.drop(columns=['Title_suffix'], inplace=True)

    merged_df_structure["Title_number"] = merged_df_structure["Title_number"].fillna(0).astype(int)
    if "Title" not in merged_df_structure.columns:
        raise ValueError("Column Title does not exist in merged_df_structure")
    # Volume_number will be 0 or 1 or 11 for example, same for Title_number, convert 

    # set df to merged_df_structure
    df = merged_df_structure.copy()

    unmatched_rows = merged_df_structure[merged_df_structure["Page_Type"].isna()]

    # Find cols only in merged_df_structure but not in df 
    new_columns = set(merged_df_structure.columns) - set(df.columns)

    temp_df = merged_df_structure[list(new_columns)].copy()
    for col in new_columns:
        if col in df.columns:
            df[col] = df[col].astype(str)
        else:
            # create col if not exist in df
            df[col] = pd.NA  

    # Correctly resetting the index and keeping the original index
    catalogue_rows = df[df["Page_Type"] == "目錄"].reset_index().rename_axis("new_index").reset_index() # = sommaire
    catalogue_rows.rename(columns={"index": "original_index", "new_index": "index"}, inplace=True)

    for _, row in catalogue_rows.iterrows():
        idx_catalogue = row["index"]  # This is the new index after resetting
        idx_original = row["original_index"]  # This is the original index in df

        if idx_catalogue in temp_df.index:
            # Fetch the row from temp_df using idx_catalogue
            temp_row = temp_df.loc[idx_catalogue]

            # Update df at the position of the original index
            for col in new_columns:
                if col in df.columns and col in temp_df.columns:
                    df.at[idx_original, col] = temp_row[col]

    pattern = r"(.)([一二三四五六七八九十百千万]+)" # number / combination of [1-10], 100, 1000, 10000
    for i in df.index:
        match = re.match(pattern, str(df["Page_Type"][i]))
        if  df["Page_Type"][i] is not None and match:
            df["Page_Type"][i] = "卷"+ match[0][1] # = rouleau / chapitre
            if n_levels > 1:
                df["Volume_number"][i] = chinese_num_to_integer(match[0][1])
                if n_subvolumes > 0:
                    for j in range(1, n_subvolumes+1):
                        df[f"Subvolume{j}_number"][i] = chinese_num_to_integer(match[0][1])
        
    for group_key, group_df in df.groupby(["Page_Type", "Volume_number"]):
        # ffill Title
        df.loc[group_df.index, 'Title'] = group_df['Title'].fillna(method='ffill')
    
        title_number = 0
        title_numbers = []  # to store title_number
        for i in group_df.index:
            if pd.notna(group_df.at[i, 'Title']):
                title_number += 1
            title_numbers.append(title_number)
        # update "Title_number" in df 
        df.loc[group_df.index, "Title_number"] = title_numbers

    # df = df[['Page_Type','Page_Number','Page_label','Title_id','Title','Title_remark','Text','Back_title','Back','Commentary','Author_name','line_labels','filename']]

    # replace wrong characters with varied version in Table of contents ("目錄")
    catalogue_df = df[df["Page_Type"] == "目錄"]

    for idx, row in df.iterrows():
        # find title according to Title_id
        catalogue_row = catalogue_df[catalogue_df['Title_id'] == row['Title_id']]
        # if title do not match
        if not catalogue_row.empty and row['Title'] != catalogue_row.iloc[0]['Title']:        
            # replace Title in df with that in Table of contents
            df.at[idx, 'Title'] = catalogue_row.iloc[0]['Title']

    # = before updating the title, check if more than one Title have the same Title_id
    suspected_ids = df.groupby('Title_id')['Title'].nunique()
    suspected_ids = suspected_ids[suspected_ids > 1].index.tolist()

    # put Title_id first column
    df = df[["Title_id"] + [col for col in df.columns if col != "Title_id"]]

    # df['filename'] = df['filename'].str.replace('-alto.xml', '', regex=False)
    return df

def process_corrected_pages_with_metadata(df: pd.DataFrame, metadata_path: str) -> pd.DataFrame:
    """
    Process the corrected pages from metadata and update the revised dataframe by making
     a match between each title and the id of each image from the ALTO files.

    Args:
        df: The revised DataFrame to be updated (i.e., the result
                                    of process_dataframe_with_numbered_index).
        metadata_path: Path to the metadata CSV file.
    Returns:
        pd.DataFrame: The updated revised DataFrame.
    """
    ## read metadata csv for page numbers
    df_corrected_pages = pd.read_csv(metadata_path)
    df_corrected_pages = df_corrected_pages[['file', 'title2']]

    df_corrected_pages['file'] = df_corrected_pages['file'].str.replace('|', ' ', regex=False)
    df_corrected_pages['file'] = df_corrected_pages['file'].str.replace('.jp2', '', regex=False)
    df_corrected_pages = df_corrected_pages.drop('file', axis=1).join(
        df_corrected_pages['file'].str.split(' ', expand=True).stack().reset_index(level=1, drop=True).rename('file')
    )
    df_corrected_pages['pages'] = None
    df_corrected_pages = df_corrected_pages.reset_index(inplace=False)[['file', 'title2', 'pages']]
    df_corrected_pages.head(50)

    for vol in df_corrected_pages.groupby('title2'):

        p_number = 1
        p_mark = 'A'

        for idx in vol[1].index:
            df_corrected_pages.loc[idx, 'pages'] = f"{p_number}{p_mark}"

            if p_mark == 'A':
                p_mark = 'B'
            else:
                p_mark = 'A'
                p_number += 1

    df_corrected_pages.head(5)

    df['pages'] = None
    for idx, row in df.iterrows():
        filename = row['filename']
        matching_pages = df_corrected_pages[df_corrected_pages['file'] == filename]['pages']
        matching_types = df_corrected_pages[df_corrected_pages['file'] == filename]['title2']
        if not matching_pages.empty: # if there is a match
            pages = matching_pages.iloc[0]
            df.at[idx, 'Pages'] = pages
            if ": " in pages:
                df.at[idx, 'Page_Type'] = pages.split(': ')[1]
            else:
                df.at[idx, 'Page_Type'] = None
        else:
            df.at[idx, 'Pages'] = None
    
    return df

def calculate_distance_point_to_line(point, line):
    # might be specific to prose but to be verified
    # todo: make it a generic function
    """
    Calculates the distance, in height, at what level the end of the polygons is located at the end of the image
    """
    line_start, line_end = line
    line_vec = np.array(line_end) - np.array(line_start)
    point_vec = np.array(point) - np.array(line_start)
    line_len = np.linalg.norm(line_vec)
    line_unitvec = line_vec / line_len
    point_vec_scaled = point_vec / line_len
    t = np.dot(line_unitvec, point_vec_scaled)
    if t < 0.0:
        t = 0.0
    elif t > 1.0:
        t = 1.0
    nearest = np.array(line_start) + line_unitvec * np.linalg.norm(line_vec) * t
    distance = np.linalg.norm(nearest - np.array(point))
    return distance

def compare_points_advanced(row: pd.Series) -> tuple:
    """
    Compare the points of the textline with the points of the block and determine if the textline starts out of 50 units from the top
    and ends out of 50 units from the bottom.

    Args:
        row: a row of the DataFrame
    
    Returns:
        tuple: A tuple containing the boolean values for 'starts_out_of_top' and 'ends_out_of_bottom'.
    """
    # definition of polygons and lines (text lines)
    block_points_str = str(row['block_points']).replace(',', ' ') if pd.notna(row['block_points']) else ''
    textline_points_str = str(row['line_points']).replace(',', ' ') if pd.notna(row['line_points']) else ''
    
    block_points = [(int(x), int(y)) for x, y in zip(block_points_str.split()[::2], block_points_str.split()[1::2])] if block_points_str else [] 
    textline_points = [(int(x), int(y)) for x, y in zip(textline_points_str.split()[::2], textline_points_str.split()[1::2])] if textline_points_str else []

    if not block_points or not textline_points:
        return False, False

    # calculate min and max of x and y (abs. and ord.) for polygon and line
    block_min_y = min(y for _, y in block_points)
    block_max_y = max(y for _, y in block_points)
    textline_min_y = min(y for _, y in textline_points)
    textline_max_y = max(y for _, y in textline_points)
    
    # calculate distances
    distance_to_top = textline_min_y - block_min_y # distance to the top of the page
    distance_to_bottom = block_max_y - textline_max_y  # distance to the bottom of the page
    
    # does it meet the conditions
    starts_out_of_top = distance_to_top > 50  # is the distance to the top greater than 50
    ends_out_of_bottom = distance_to_bottom > 50  # is the distance to the bottom greater than 50
    
    return starts_out_of_top, ends_out_of_bottom

def calculate_and_number_paragraphs(df: pd.DataFrame) -> pd.DataFrame:
    """
    Calculate the number of textlines starting out of 50 units from the top and ending out of 50 units from the bottom.
    Assign paragraph numbers to textlines based on specified conditions.

    Args:
        df: DataFrame containing the data.

    Returns:
        pd.DataFrame: DataFrame with added columns for 'starts_out_of_top', 'ends_out_of_bottom',
                     'paragraph_ends', and 'text_para_number'.
    """
    # Apply method to compare points and create new columns
    df[['starts_out_of_top', 'ends_out_of_bottom']] = df.apply(compare_points_advanced, axis=1, result_type='expand')

    # Count elements that do not meet the conditions
    starts_out_of_top_count = df['starts_out_of_top'].sum()
    ends_out_of_bottom_count = df['ends_out_of_bottom'].sum()

    # Print results
    logger.info(f"Textlines starting out of 50 units from the top: {starts_out_of_top_count}")
    logger.info(f"Textlines ending out of 50 units from the bottom: {ends_out_of_bottom_count}")

    # Assign 'paragraph_ends' based on 'ends_out_of_bottom'
    df['paragraph_ends'] = df['ends_out_of_bottom']

    # Initialize 'text_para_number' column
    df['text_para_number'] = None

    # Iterate over groups based on 'Title_id' excluding '目錄'
    for file in df.loc[df['Page_Type'] != '目錄'].groupby('Title_id'):
        para_number = 1
        for i in file[1].index:
            if df['Text'][i] and df['line_labels'][i] == 'Text':
                df.at[i, 'text_para_number'] = para_number
                if df['paragraph_ends'][i]:
                    para_number += 1

    # Reorder columns in the DataFrame
    df = df[['Page_Type', 'Pages', 'Title_id', 'Title', 'Title_remark', 'Text', 'Back_title', 'Back', 'Commentary', 'Author_name', 'line_labels', 'paragraph_ends', 'text_para_number', 'filename']]

    return df

def group_and_log_documents(df: pd.DataFrame, output_dir: Path, exclude_page_types: List[str] = ['目錄']) -> None:
    """
    Process a DataFrame of file information and organize output into specified directories.

    Args:
        df: The DataFrame containing file information.
        output_dir: The directory where output files will be saved.
        exclude_page_types: List of page types to be excluded from processing.
        specific_terms: Dictionary containing specific terms to be used in processing. (e.g. Chinese terms specific for front and preface)
                                         Default = {'front_part': '前部','preface': '敘'}.
    
    Returns:
        None
    """
    
    if not output_dir.exists():
        output_dir.mkdir(parents=True, exist_ok=True)
        logger.info(f"Created directory: {output_dir}")

    # Replace NaNs with None
    df = df.apply(lambda x: x.map(lambda y: None if pd.isna(y) else y))
    file_name_counter = 0

    # Log and process files except those with specific Page_Type
    for title_id, group in df.loc[~df['Page_Type'].isin(exclude_page_types)].groupby('Title_id'):
        # Prepare basic information
        file_name_counter += 1
        unique_authors = group['Author_name'].dropna().unique()
        author = unique_authors[0] if len(unique_authors) > 0 else "No Author"

        logger.info(f"Title ID: {title_id}, File Name Counter: {file_name_counter}, Length: {len(group)}, "
                    f"Volume: {group['Page_Type'].unique()[0]}, Title: {group['Title'].unique()[0]}, Author: {author}")

    # Further processing for files excluding specific Page_Types
    for title_id, group in df.loc[~df['Page_Type'].isin(exclude_page_types)].groupby('Title_id'):
        logger.info(f"Processing Title ID: {title_id}")

    # Group by 'Page_Type' and get the last 'Pages' and 'filename' value in each group
    last_pages_per_type = df.groupby('Page_Type')['Pages'].last()
    last_images_per_type = df.groupby('Page_Type')['filename'].last()

    # Log the results
    logger.info(f"Last pages per type: {last_pages_per_type}")
    logger.info(f"Last images per type: {last_images_per_type}")
    logger.info(f"Filenames in the last group: {group['filename']}")
