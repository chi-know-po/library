import argparse
import yaml
from typing import Dict, List, Literal, Optional, Union
from pydantic import BaseModel, ValidationError

class DigRole(BaseModel):
    """
    Digital contribution roles defined by NISO.
    """
    dig_role: Literal[
        "Conceptualization", "Formal Analysis", "Data Curation", 
        "Funding Acquisition", "Methodology", "Project Administration", 
        "Resources", "Supervision", "Validation", "Visualization"
    ]
    forename_alph: str # mandatory. Needs to be retrieved from database
    surname_alph: str # mandatory. Needs to be retrieved from database
    alph_lang: str = "en" # mandatory, always english in the CKP project
    forename_orig: Optional[str] = None # mandatory except for some research assistants. Needs to be retrieved from database
    surname_orig: Optional[str] = None # mandatory except for some research assistants. Needs to be retrieved from database
    orig_lang: str = "zh" # mandatory, always Chinese in the CKP project
    # idno = Author id in our database when type = CKP. ORCID id elsewhere. Should also be stored in the database, in the sources table.
    # CKP for authors from the_people table, ORCID for authors from table sources
    idno: str 
    idno_type: Literal["ORCID", "CKP"]

class TradRole(BaseModel):
    """
    Traditional roles based on IdRef.
    """
    trad_role: Literal["Author", "Editor", "Compiler", "Commentator"]
    forename_alph: str # mandatory. Needs to be retrieved from database
    surname_alph: str # mandatory. Needs to be retrieved from database
    alph_lang: str = "en" # mandatory, always English in the CKP project
    forename_orig: Optional[str] # mandatory except for some research assistants. Needs to be retrieved from database
    surname_orig: Optional[str] # mandatory except for some research assistants. Needs to be retrieved from database
    orig_lang: str = "zh" # mandatory, always Chinese in the CKP project
    idno: str # Author id in our database when type = CKP. ORCID id elsewhere. Should also be stored in the database, in the sources table.
    idno_type: Literal["CKP", "ORCID"] # CKP for authors from table the_people, ORCID for authors from table sources

class Edition(BaseModel):
    """
    Information about the edition.
    """
    funder: List[str] = ["USIAS", "Collex-Persée"] # mandatory, constant
    authorities: List[str] = ["Université de Strasbourg", "Estrades"] # mandatory, constant
    pub_place: str = "France" # mandatory, constant
    pub_date: int = 2024 # mandatory, constant
    DOI: str = "00.0000/zenodo.0000000" # when the file is uploaded, it will be given a DOI, until then, use a placeholder
    status: str = "pending" # mandatory, constant
    license: str = "CC-BY-NC-SA-4.0" # mandatory
    editorial_decl_eng: str # mandatory, to be written for each book
    cor_status: str = "low" # mandatory, constant
    cor_presentation_eng: str = (
        "This edition mainly aims at bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. base text, title, prefaces, commentaries, and other annotations. The editorial work also includes orthographic emendation and adding of modern punctuation (see below). Finally, the text is enriched through the identification of quotations." # mandatory and constant
    )
    punct_decl_eng: str = (
        "Modern punctuation is added to the text as often as possible. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」." # mandatory and constant
    )
    quote_decl_eng: str = (
        "As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using 'cit' tag, followed by 'ref' if source is identified, and by 'quote' for quoted text material." # mandatory and constant
    )
    front_decl_eng: str = (
        '"front" is used at the beginning of the file. It generally includes the title of the text per se. It may also include a preface.' # mandatory, constant for all books except the Shijing
    )
    div_decl_eng: str = (
        '"div" is used in the "front" section to separate the introductory texts (title, prefaces and commentaries). In the "body" section, it is used to distinguish base text, commentaries. Commentaries are also subdivided ("div") depending on the line, the group of lines or the stanza it refers to.' # mandatory, constant for all books except the Shijing
    )
    seg_decl_eng: str = (
        '"seg" is used to delineate segments of text, specific commentaries point to these segments of text.' # mandatory, constant for all books except the Shijing
    )
    lb_decl_eng: str = (
        '"lb" indicates line beginning on the facsimile of the reference edition.' # mandatory, constant for all books except the Shijing
    )
    back_decl_eng: str = (
        '"back" is used to tag a coda when one is to be found at the end of a text.' # mandatory, constant for all books except the Shijing
    )

class Identifier(BaseModel):
    """
    Identifiers for the document.
    """
    idno_type: Literal["DOI", "CKP", "Call Number", "IdRef", "ORCID", "URL"] # mandatory
    library_id: str = "Bibliothèque des Hautes études chinoises - Collège de France (031909841)" # mandatory, library where the book (and the series) is stored

class Series(BaseModel):
    """
    Series information, if present.
    """
    series_title: str # this appears in the bibliographical description of the library catalogue
    series_title_alt: str # Pinyin transliteration of the series_title
    series_callnumber: str # series call number

class TextTime(BaseModel):
    """
    Temporal information about the text.
    """
    text_start: int
    text_end: int
    text_period: Optional[str] = "" # e.g. 宋元

class Source(BaseModel):
    """
    Information about the source.
    """
    source_publisher: str # mandatory. Comes from the library catalogue most of the time
    source_pub_place: List[str] = ["China", "Shanyang 山陽"] # mandatory. All editions were produced in some place in China
    source_date_trad: str # mandatory. It is a Chinese date so it is a string
    source_date_conv: int # mandatory. Comes from the library catalogue
    source_description_eng: Optional[str] = None # optional.
    source_description_chin: Optional[str] = None # optional.
    genre: str = "論"
    style: str = "prose" # [prose, verse] # either one of the two
    text_time: TextTime # temporal information about the text

class AppInfo(BaseModel):
    """
    Application information, mandatory for all editions using this pipeline.
    """
    app_label: str = "Alto2TEI" # mandatory for all editions using this pipeline, constant
    app_version: str = "1.0"
    app_id: Optional[str] = None # if empty, do not document
    app_target: str = "https://gitlab.huma-num.fr/chi-know-po/library" # mandatory for all editions using this pipeline, constant

class TEIMetadata(BaseModel):
    """
    Root TEI Metadata model.
    """
    datatype: List[str] = ["books", "http://purl.org/coar/resource_type/c_2f33/"]
    # datatype should be a Tuple, but as we use YAML we prefer to use a List
    # it has always two elements, the datatype and the URI (see https://purl.org/coar/resource_type/)
    file_stem: str # mandatory
    title_main: str # mandatory
    title_main_lang: str = "lzh" # mandatory
    title_alt: str = "Commentary on Flora and Fauna in the Poems in the Mao Tradition (An electronic version)" # mandatory
    title_alt_lang: str = "en" # mandatory
    title_main_short: Optional[str] = None # optional
    title_alt_short: Optional[str] = None # optional
    contributors: List[Union[DigRole, TradRole]] # contributors information
    edition: Edition # edition information
    identifiers: Identifier # identifiers information
    series: Optional[Series] = None # series information, if present
    collections: Optional[Dict[str, str]] = {} # optional (collections to which the text belongs), e.g. {"CKP": "10.34847/nkl.0d78glhq"}
    source: Source # source information
    app_info: AppInfo # application information
    splitters: Optional[List[str]] = [] # List of files where the text should be split, if any
    exclude_page_types: Optional[List[str]] = [] # optional (list of page types to exclude from the TEI file)
    rights: Optional[Dict[str, str]] = {} # optional (rights information for users), e.g. {"MBizais": "ROLE_OWNER"}
    description_orig: Optional[str] = None # optional (description in the original language)
    description_alt: Optional[str] = None # optional (description in English)
    version: str = "1.0" # mandatory
    format: Optional[str] = None # optional
    conformsTo: str = "TEI" # mandatory
    publisher: str # mandatory, e.g. "Conceptualization-Funding acquisition-Investigation-Supervision-Visualization: Researcher A (ORCID: 0000-000X-XXXX-XXXX)|Supervision: Researcher B (ORCID: 0000-000X-XXXX-XXXX)|Funding acquisition-Project Administration: Institution A|Resources-Project Administration-Supervision-Data Curation-Visualization: Institution B|Resources: Institution C"
    keywordsOrig: Optional[List[str]] = [] # optional (keywords in the original language)
    keywordsAlt: Optional[List[str]] = [] # optional (keywords in the alternative language)

def parse_yaml_to_metadata(yaml_file_path: str) -> TEIMetadata:
    """
    Parse a YAML file and create a TEIMetadata object.

    Args:
        yaml_file_path (str): Path to the YAML file to be parsed.

    Returns:
        TEIMetadata: An object containing the parsed metadata.
    """
    with open(yaml_file_path, "r", encoding="utf-8") as file:
        data = yaml.safe_load(file)

    contributors = []
    for contrib in data["contributors"]:
        if "dig_role" in contrib:
            contributors.append(DigRole(**contrib))
        else:
            contributors.append(TradRole(**contrib))

    edition = Edition(**data["edition"])
    identifiers = Identifier(**data["identifiers"])
    source = Source(**data["source"])
    app_info = AppInfo(**data["app_info"])
    series = Series(**data["series"]) if "series" in data else None

    metadata = TEIMetadata(
        datatype=data["datatype"],
        file_stem=data["file_stem"],
        title_main=data["title_main"],
        title_main_lang=data["title_main_lang"],
        title_alt=data["title_alt"],
        title_alt_lang=data["title_alt_lang"],
        title_main_short=data.get("title_main_short"),
        title_alt_short=data.get("title_alt_short"),
        contributors=contributors,
        edition=edition,
        identifiers=identifiers,
        series=series,
        source=source,
        app_info=app_info,
        exclude_page_types=data["exclude_page_types"],
        splitters=data["splitters"],
        collections=data["collections"],
        rights=data["rights"],
        description_orig=data["description_orig"],
        description_alt=data["description_alt"],
        version=data["version"],
        format=data["format"],
        conformsTo=data["conformsTo"],
        publisher=data["publisher"],
        keywordsOrig=data["keywordsOrig"],
        keywordsAlt=data["keywordsAlt"]
    )

    return metadata

if __name__ == "__main__":
    # # Check validator
    # try:
    #     tei_metadata = TEIMetadata(
    #         title_main="毛詩草木鳥獸蟲魚疏", 
    #         title_main_ref="毛詩草木鳥獸蟲魚疏",
    #         title_main_lang="lzh",
    #         title_alt="Commentary on Flora and Fauna in the Poems in the Mao Tradition (An electronic version)",
    #         title_alt_lang="en",
    #         contributors=[
    #             DigRole(
    #                 dig_role="Conceptualization",
    #                 forename_alph="John",
    #                 surname_alph="Doe",
    #                 alph_lang="en",
    #                 forename_orig="约翰",
    #                 surname_orig="多伊",
    #                 orig_lang="zh",
    #                 idno="0000-0001-2345-6789",
    #                 idno_type="ORCID"
    #             ),
    #             TradRole(
    #                 trad_role="Author",
    #                 forename_alph="Jane",
    #                 surname_alph="Smith",
    #                 alph_lang="en",
    #                 forename_orig="简",
    #                 surname_orig="史密斯",
    #                 orig_lang="zh",
    #                 idno="CKP123456",
    #                 idno_type="CKP"
    #             )
    #         ],
    #         edition=Edition(
    #             funder=["USIAS", "Collex-Persée"],
    #             authority="Université de Strasbourg",
    #             pub_place="France",
    #             pub_date=2024,
    #             status="restricted",
    #             license_target="https://creativecommons.org/licenses/by-sa/4.0/",
    #             license="Creative Commons CC-BY-NC-SA (Attribution-NonCommercial-ShareAlike) 4.0 International Public License",
    #             editorial_decl_eng="This is an electronic edition of the 毛詩草木鳥獸蟲魚疏...",
    #             cor_status="low",
    #             cor_presentation_eng="This edition mainly aims at bringing to light three elements...",
    #             punct_decl_eng="Modern punctuation is added to the text as often as possible...",
    #             quote_decl_eng="As often as possible, each citation is linked to its source...",
    #             front_decl_eng='"front" is used at the beginning of the file...',
    #             div_decl_eng='"div" is used in the "front" section...',
    #             seg_decl_eng='"seg" is used to delineate segments of text...',
    #             lb_decl_eng='"lb" indicates line beginning on the facsimile of the reference edition...',
    #             back_decl_eng='"back" is used to tag a coda when one is to be found at the end of a text.'
    #         ),
    #         identifiers=[
    #             Identifier(
    #                 idno_type=["DOI", "CKP", "Call Number", "IdRef", "ORCID", "URL"],
    #                 library_id="Bibliothèque des Hautes études chinoises - Collège de France (031909841)"
    #             )
    #         ],
    #         source=Source(
    #             source_publisher="Dingshi liuyi tang丁氏六藝堂",
    #             source_pub_place=["China", "Shanyang 山陽"],
    #             source_date_trad="Qing Tongzhi yuan nian 清同治元年",
    #             source_date_conv=1862
    #         ),
    #         text_time=TextTime(
    #             text_start=1183,
    #             text_end=1350,
    #             text_period="宋元"
    #         app_info=AppInfo(
    #             app_label="Alto2TEI",
    #             app_target="https://gitlab.huma-num.fr/chi-know-po/library"
    #         )
    #     )
    #     print(tei_metadata.model_dump_json(indent=2))
    # except ValidationError as e:
    #     print(f"Error: {e}")
    
    # From YAML file:
    parser = argparse.ArgumentParser(description="Parse a YAML file and create a TEIMetadata object.")
    parser.add_argument("yaml_file_path", type=str, help="Path to the YAML file to be parsed.")
    args = parser.parse_args()
    with open(args.yaml_file_path, "r", encoding="utf-8") as f:
        metadata = yaml.load(f, Loader=yaml.FullLoader)
    try:
        tei_metadata = TEIMetadata(**metadata)
        print(tei_metadata.model_dump_json(indent=2))
    except ValidationError as e:
        print(f"Error: {e}")
