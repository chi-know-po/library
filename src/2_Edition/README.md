# XML Edition (ALTO to TEI)

## Description

This folder contains scripts related to XML Edition, which focuses on the transformation of **Analyzed Layout and Text Object (ALTO)** files into fully compliant **Text Encoding Initiative (TEI)** documents.  

## Structure

> Note: "`TITLE`" in the paths stands for the corpus you on from the data folder. For LJ Shu for example, the correct paths are `../../data/LJ_Shu` 

- **6 steps** constitute the pipeline:
  1. `1_Create_Metadata_csv.ipynb` Notebook agregates file names and corresponding metadata for all parts of the book that is being edited (general structure of the book).  
      **Input**:  
         - `TITLE.yaml` (YAML metadata file)  
         - `Altos/`  
      **Output**:  
         - `TITLE_metadata.csv`  
     **/!\ Recommended verification:** The metadata file can contain small structure mistakes, if so correct them and generate another file that ends with `_ok.csv`.
  2. `2_Alto2CSV_to_check.ipynb` Notebook converts the alto into a csv file that should be curated manually (orthographic emendation and structure of the text).  
     **Input**: XML files in `Altos/` directory  
     **Output**: `TITLE_to_check.csv`  
     **/!\ Recommended verification:** After this step, it is recommended to generate a `_checked.csv` version of the output CSV by manually correcting the possible mistakes.
  2. `3_Create_TOC.ipynb` Notebook creates a table of content (TOC) from the curated csv file.  
      **Input**:  
         - `TITLE_to_check.csv` &rarr; if no `checked` is given, `to_checked` will be used    
         - `TITLE_checked.csv` &rarr; if available, use this   
         - `TITLE.yaml`   
      **Output**:  
         - `TITLE_toc.csv`  
      **/!\ Recommended verification:** While the table of contents should not contain any mistake, it is recommanded to produce a `_toc_ok.csv` version by reviewing the file. If no correction is needed, the `_toc.csv` file will be used in the rest of the pipeline.  
  3. `4_Create_Structure_csv.ipynb` Notebook extracts the table of contents of the book based on the `checked` csv (or, if it is not available, the `to_check` csv). This structure will be mirrored in the tree of files of the TEI-Corpus edition. This step should not necessitate any curation. However, checking the file is more secure.  
     **Input**:  
         - `TITLE_toc.csv` &rarr; if no `toc_ok` is given, `toc` will be used    
         - `TITLE_toc_ok.csv` &rarr; if available, use this   
     **Output**:  
         - `TITLE_structure.csv`  
  4. `5_Concat2Numbered_csv.ipynb` Notebook aligns and merges checked and structure_ok csv files to integrate text within a predefined structure.  
     **Input**:  
         - `TITLE_structure.csv`  
         - `TITLE_to_check.csv` &rarr; if no `checked` is given, `to_checked` will be used  
         - `TITLE_checked.csv` &rarr; if available, use this  
         - `TITLE_metadata.csv` &rarr; if no `metadata_ok` is given, `metadata` will be used  
         - `TITLE_metadata_ok.csv` &rarr; if available, use this  
     **Output**:  
         - `TITLE_numbered.csv`
  5. `6_Generate_TEI.ipynb` Notebook generates all the TEI files for the edition (including a header).  
     **Input**:  
         - `TITLE_numbered.csv`  
         - `TITLE_metadata.yaml`  
     **Output**:  
         - all XML files in `TEI/` directory  

  i.e. `tei_production.py` can be launched independently for step 6 of the pipeline with the following command :
   ```bash
   python tei_production.py
   ```  
- **`*.py`**: Python scripts containing the library used in the notebook for the transformation process. As for the rest of the pipeline, the data configuration must be correctly entered as indicated below.

## Execution

The pipeline is designed to convert ALTO XML files to TEI XML files. It also requires metadata to produce the header and the tree for the TEI XML files.

### Prerequisites (environment and data configuration)

Ensure you are working within the project's environment. All necessary libraries should be installed for this environment.

**Before running the pipeline, you need to select and point out the corpus you are working on (and for it you need to have at least the two mandatory inputs listed below). To select it, follow the steps below.**  
1. Edit `data_config.yaml` in the current folder (`src/edition/data_config.yaml`  
2. In the *corpus section*, indicate the different corpus you intend to work on. If you intend to work on a new corpus, it needs to be added in this section with its given name as key and relative path as value.
    - Note: the path here is relative and depends on the data folder selected. If None given in the params, then the default data folder will be selected (located in `data/`) 
3. At the top of this document, indicate which corpus you want to work on (i.e. *selected_corpus*). It needs to be a key from the *corpus section*.
4. If you want to have specific parameters for this corpus, go to the *params section* and create an entry for your data. Follow the existing templates to modify any parameter. 
    - Note: If you want to use another data folder than the one by default, ensure that its name and path are in the corpus section above.




### Mandatory and optional inputs

All input and output paths can be changed in the data configuration file mentioned in the above *Prerequisties* subsection. If none are given then those input paths will be selected:

In all cases (mandatory):
- `TITLE_metadata.yaml`  (YAML with metadata informations)
- `TITLE_ALTO/` (folder containing all ALTO files)

If they have been checked (optional):
- `TITLE_checked.csv` (rather than the automatically generated `TITLE_to_check.csv`)  
- `TITLE_toc_ok.csv` (rather than the automatically generated `TITLE_toc.csv`)
- `TITLE_structure_ok.csv` (rather than the automatically generated `TITLE_structure.csv`)


### Execution Steps


1. **Document** (manually or at least separately) `TITLE_metadata.yaml` file with all the information needed and **provide** ALTO XML files in the data directory. A model is provided for the yaml file.
2. **Run** `1_Create_Metadata_csv.ipynb`.
3. <u>(not mandatory but recommended)</u> **check manually** the output file and save it as `TITLE_metadata_ok.csv`.
4. **Run** `2_Alto2CSV_to_check.ipynb`.
5. <u>(not mandatory but recommended)</u> **check manually** the output file and save it as `TITLE_checked.csv`.
6. **Run** `3_Create_TOC.ipynb`. 
7. <u>(not mandatory but recommended)</u> **check manually** the output file and save it as `TITLE_toc_ok.csv`.
8. **Run** `4_Create_Structure_csv.ipynb`.
9. <u>(not mandatory *as the TOC file should cover any mistakes from structure* but we found that a verification here could be of use)</u> **check manually** the output file and save it as `TITLE_structure_ok.csv`.
10.  **Run** `5_Concat2Numbered_csv.ipynb`
11. **Run** `6_Generate_TEI.ipynb`.

Execute all of the Notebook cells to convert the ALTO to a folder of XML TEI files. This folder should contain a root file (`TITLE.xml`) and a hierarchy of text files depending on the way the book's structure is conceived.

By following these steps, you should successfully convert ALTO XML files to TEI XML files. If you encounter any error, please document them in the Git issues section.
