import pandas as pd
import yaml
from loguru import logger
from pathlib import Path
from pydantic import BaseModel, ValidationError

class CorpusPathObject(BaseModel):
    """
    Model representing the paths for a given corpus.
    """
    corpus_name: str
    base_folder: Path
    alto_folder: Path
    yaml_metadata: Path
    checked: Path = None
    structure_ok: Path = None
    toc_ok: Path = None
    metadata_ok: Path = None
    to_check: Path = None
    metadata: Path = None
    numbered: Path = None
    toc: Path = None
    structure: Path = None
    tei_folder: Path = None

def load_yaml_config(yaml_path: Path):
    """
    Load YAML configuration from the specified path.

    Args:
        yaml_path (Path): Path to the YAML configuration file.

    Returns:
        dict: Parsed YAML configuration as a dictionary.
    """
    with open(yaml_path, 'r') as file:
        config = yaml.safe_load(file)
    return config

def generate_default_paths(corpus_name: str, base_folder: Path):
    """
    Generate default paths for the specified corpus.

    Args:
        corpus_name (str): Name of the corpus.
        base_folder (Path): Base folder where the corpus data is stored.

    Returns:
        dict: A dictionary containing default paths for various corpus files and folders.
    """
    return {
        'corpus_name': corpus_name,
        'base_folder': base_folder,
        'alto_folder': base_folder / f"{corpus_name}_ALTO/",
        'yaml_metadata': base_folder / f"{corpus_name}_metadata.yaml",
        'checked': base_folder / f"{corpus_name}_checked.csv",
        'structure_ok': base_folder / f"{corpus_name}_structure_ok.csv",
        'toc_ok': base_folder / f"{corpus_name}_toc_ok.csv",
        'metadata_ok': base_folder / f"{corpus_name}_metadata_ok.csv",
        'to_check': base_folder / f"{corpus_name}_to_check.csv",
        'metadata': base_folder / f"{corpus_name}_metadata.csv",
        'numbered': base_folder / f"{corpus_name}_numbered.csv",
        'toc': base_folder / f"{corpus_name}_toc.csv",
        'structure': base_folder / f"{corpus_name}_structure.csv",
        'tei_folder': base_folder / f"{corpus_name}_TEI/",
    }

def validate_paths(alto_folder: Path, yaml_metadata: Path):
    """
    Validate the existence and format of mandatory paths (ALTO folder and YAML metadata file).

    Args:
        alto_folder (Path): Path to the ALTO folder, which should contain XML files.
        yaml_metadata (Path): Path to the YAML metadata file.

    Raises:
        FileNotFoundError: If the folder or file does not exist or is not in the correct format.
    """
    # Check if alto_folder exists and contains XML files
    if not alto_folder.exists() or not any(alto_folder.glob('*.xml')):
        logger.warning(f"The ALTO folder '{alto_folder}' does not exist or contains no XML files.")
    # Check if yaml_metadata exists and is a .yaml file
    if not yaml_metadata.exists() or yaml_metadata.suffix != '.yaml':
        raise FileNotFoundError(f"The metadata file '{yaml_metadata}' does not exist or is not a .yaml file.")

class CorpusPathsLoader:
    """
    Class responsible for generating and validating paths for a selected corpus.
    """
    def __init__(self, yaml_config_path: Path):
        """
        Initialize the CorpusPathsLoader with the path to the YAML configuration file.

        Args:
            yaml_config_path (str): Path to the YAML configuration file.
        """
        self.yaml_path = yaml_config_path.resolve()
        self.config = load_yaml_config(self.yaml_path)
        # Ensure selected_corpus is defined before accessing it
        self.selected_corpus = self.config['selected_corpus']
        self.base_data_folder = self.get_base_data_folder()
        self.selected_corpus = self.config['selected_corpus']
        self.paths = None

    def get_base_data_folder(self):
        """
        Determine the base data folder to use based on the configuration.

        Returns:
            Path: The resolved path to the base data folder.
        """
        if 'params' in self.config and self.selected_corpus in self.config['params']:
            params = self.config['params'][self.selected_corpus]
            if params is not None and 'data_folder' in params:
                data_folder_key = params['data_folder']
                if data_folder_key in self.config['data_folder']:
                    return (self.yaml_path.parent / self.config['data_folder'][data_folder_key]).resolve()
        # Fallback to default data folder
        return (self.yaml_path.parent / self.config['data_folder']['default_data_folder']).resolve()

    def generate_paths(self):
        """
        Generate paths for the selected corpus, using either specified or default values.

        Returns:
            CorpusPathObject: An object containing all generated paths for the corpus.

        Raises:
            ValueError: If the selected corpus is not defined in the corpus section of the configuration.
            ValidationError: If any of the paths are not valid.
        """
        if self.selected_corpus not in self.config['corpus']:
            raise ValueError(f"Selected corpus '{self.selected_corpus}' is not defined in the corpus section.")
        
        corpus_base_folder = self.base_data_folder / self.config['corpus'][self.selected_corpus]

        # Check if params for the selected corpus exist
        if 'params' in self.config and self.selected_corpus in self.config['params']:
            params = self.config['params'][self.selected_corpus]
            # Use specified paths, otherwise generate defaults
            self.paths = {}
            default_paths = generate_default_paths(self.selected_corpus, corpus_base_folder)
            for key, default_value in default_paths.items():
                if params is not None and key in params and params[key]:
                    self.paths[key] = corpus_base_folder / params[key]
                else:
                    self.paths[key] = default_value
        else:
            # Generate all paths by default
            self.paths = generate_default_paths(self.selected_corpus, corpus_base_folder)

        # Validate mandatory paths
        validate_paths(self.paths['alto_folder'], self.paths['yaml_metadata'])

        # Create CorpusPathObject
        try:
            corpus_paths = CorpusPathObject(**self.paths)
            logger.info("Corpus paths successfully validated and created:")
            logger.info(corpus_paths)
            return corpus_paths
        except ValidationError as e:
            logger.error("Validation error:", e)
            raise e

if __name__ == "__main__":
    data_config_path = Path("src/Edition/data_config.yaml")
    generator = CorpusPathsLoader(data_config_path)
    corpus_paths = generator.generate_paths()

    for key, value in corpus_paths.model_dump().items():
        print(f"{key}: {value}")
