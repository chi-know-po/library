
"""
Objective: 
Import "output/LJ_shu_to_check.csv" as a DataFrame and guess the structure of the data.
If in the column "Page_Type" we found one or more occurrences of "目錄" (Table of Contents), log that we found it and kill the program.
Else:
Add an index column to the DataFrame. 
Create a new dataframe that contains the following columns (already in the original DataFrame):
- "Title"
- "Title_remark"
- "line_labels"
- "block_labels"
- "Page_Type"
- "Page_Number"
If the column "Title" and "Title_remark" are empty, remove the row.
Else, keep the row with the original index.
"""
import pandas as pd
from loguru import logger
from pathlib import Path

# Define the file path for input and output
input_file_path = Path("output/LJ_shu_to_check.csv")
output_file_path = Path("") 
# if no output given, output to input file path
if output_file_path == Path(""):
    output_file_path = input_file_path

if __name__ == "__main__":
    # Check if the input file exists before proceeding
    if input_file_path.exists():
        # Load the CSV file into a DataFrame
        df = pd.read_csv(input_file_path)

        # Check if any occurrence of "目錄" is present in the "Page_Type" column
        if "目錄" in df["Page_Type"].values:
            # count the number of occurrences of "目錄" in the "Page_Type" column
            count = df["Page_Type"].str.contains("目錄").sum()
            logger.info(f"Found '目錄' in Page_Type. Text data structure contains {count} tables of contents.\nFile \"{input_file_path.name}\" is not to be modified.")
        else:
            # Log the initial count of '目錄' in the "Page_Type" column before any changes
            original_count = df["Page_Type"].str.contains("目錄").sum()
            logger.info(f"Initial count of '目錄' in Page_Type: {original_count}")

            
            # Localise the number of lines where either "Title" or "Title_remark" is populated
            title_lines = df.loc[(df["Title"].str.len() > 0) | (df["Title_remark"].str.len() > 0)]
            title_lines_count = title_lines.shape[0]


            # Change text of "Page_Type" to "目錄" for title_lines in df
            df.loc[title_lines.index, "Page_Type"] = "目錄"
            # count the number of occurrences of "目錄" in the "Page_Type" column
            count = df["Page_Type"].str.contains("目錄").sum()
            
            # Save the resulting DataFrame to a CSV file
            df.to_csv(output_file_path, index=False)
            # count the number of occurrences of "目錄" in the "Page_Type" column for both DataFrames
            count = df["Page_Type"].str.contains("目錄").sum()
            logger.info(f"Found '目錄' in Page_Type. Text data structure contains {count} tables of contents.")
    else:
        logger.error(f"File {input_file_path} not found. Exiting.")

    