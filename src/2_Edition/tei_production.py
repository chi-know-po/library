import argparse
import pandas as pd
import pypinyin
import re
import sys
from lxml import etree as ET
from loguru import logger
from pathlib import Path
sys.path.append(str(src := Path(__file__).resolve().parents[1])) # add the src directory to sys.path
from typing import Any, Dict, List, Optional, Tuple, Union
from xml.dom.minidom import parseString

from tei_models import TEIMetadata, DigRole, TradRole, parse_yaml_to_metadata
from utils.numeral_conversion import chinese_num_to_integer

xml_ns = "http://www.w3.org/XML/1998/namespace" # Namespace for XML (W3C XML namespace).
tei_ns = "http://www.tei-c.org/ns/1.0" # Namespace for TEI (Text Encoding Initiative).
xi_ns = "http://www.w3.org/2001/XInclude" # Namespace for XInclude (W3C XInclude specification).

class TEIFileGenerator:
    """
    A class to generate TEI files from structured data.

    Methods:
        __init__(self, logger_verbose: bool = False): Initialize the TEIFileGenerator object.
        set_logger(self, verbose: bool): Set the logger verbosity level.
        generate_tei_files(self, df: pd.DataFrame, metadata: TEIMetadata, output_dir: Path): Wrapper function to process the data and create TEI files.
        create_root_teiCorpus(metadata: TEIMetadata) -> ET.Element: Create the root teiCorpus element for the TEI document.
        create_section_tei_header(metadata: TEIMetadata) -> ET.Element: Create the TEI header element for a section document.
        create_individual_tei_header(metadata: TEIMetadata) -> ET.Element: Create the TEI header element for an individual document.
        prettify_xml(tree: ET.ElementTree, indentation: str = "    ") -> str: Prettify the XML content of an ElementTree.
        get_first_non_none(*args: Any) -> Optional[Any]: Returns the first non-None value from the given arguments.
        create_paragraphs_from_lines(lines: List[Dict[str, Any]]) -> List[ET.Element]: Create paragraphs from a list of lines.
        create_paragraphs_from_commentary(lines: List[Dict[str, Any]]) -> List[Tuple[ET.Element, int]]: Create paragraphs of commentary from a list of lines.
    """

    def __init__(self, logger_verbose: bool = False):
        """
        Initialize the TEIFileGenerator object, set logger verbosity level.

        Args:
            logger_verbose (bool): Set the logger verbosity level to INFO if True, else set to WARNING
        """
        self.set_logger(logger_verbose)

    def set_logger(self, verbose: bool):
        if verbose:
            logger.remove()
            logger.add(sys.stderr, level="INFO")
        else:
            logger.remove()
            logger.add(sys.stderr, level="WARNING")

    def generate_tei_files(self, df: pd.DataFrame, metadata: TEIMetadata, output_dir: Path):
        """
        Wrapper function to process the data and create TEI files.

        Args:
            df (pd.DataFrame): DataFrame containing the data to be processed
            metadata (TEIMetadata): Metadata object containing information for the TEI header
            output_dir (Path): Path to the output directory
        """
        # Remove NaN values from the DataFrame and reset index
        df_revised = df.map(lambda x: None if pd.isna(x) else x)
        df_revised = df_revised.reset_index(drop=True)

        # Find the last Pages value for every volume
        last_pages_per_type = df_revised.groupby("Page_Type")["Pages"].last()
        last_images_per_type = df_revised.groupby("Page_Type")["filename"].last()
        # count the number of sections in the dataframe (i.e. Take the first two characters of the title_id and count the unique values minus the root)
        num_sections = len(df_revised["Title_id"].str[:2].unique()) - 1
        
        # Iterate over each group in df_revised that is not in the excluded Page_Type categories
        # if no title_id = "00-00", add line "00-00" to the dataframe
        if "00-00" not in df_revised["Title_id"].unique():
            df_revised = pd.concat([df_revised, pd.DataFrame([{"Title_id": "00-00"}])], ignore_index=True)
            # fill values for the new line with ""
            df_revised.loc[df_revised["Title_id"] == "00-00", ["Title", "Author_name", "Text", "Commentary", "Back", "paragraph_ends", "filename"]] = ""

        # create a dictionary with the section numbers as keys (i.e. the characters before the hyphen "-")
        # and as values the number of subsections (i.e. the characters after the hyphen "-")
        title_ids = df_revised["Title_id"].unique()
        titles_dict = {title_id.split("-")[0]: title_id.split("-")[1] for title_id in title_ids}
        # create a list of all sections that do not have a "00" subsection
        sections_without_root_lines = [title_id for title_id in titles_dict.keys() if titles_dict[title_id] != "00"]
        for section in sections_without_root_lines:
            df_revised = pd.concat([df_revised, pd.DataFrame([{"Title_id": f"{section}-00"}])], ignore_index=True)
            # fill values for the new line with ""
            df_revised.loc[df_revised["Title_id"] == f"{section}-00", ["Title", "Author_name", "Text", "Commentary", "Back", "paragraph_ends", "filename"]] = ""
        for file in df_revised.loc[~df_revised["Page_Type"].isin(metadata.exclude_page_types or [])].groupby("Title_id"):
            title_id = file[0]
            page_type = "root" if title_id == "00-00" else "section" if title_id.endswith("-00") else "text"
            line = file[1]

            # Determine the primary author from unique values
            unique_authors = line["Author_name"].dropna().unique()
            author = unique_authors[0] if len(unique_authors) > 0 else "No Author"

            logger.info(f"{title_id}  title_id: {title_id}  length: {len(line)}  Vol: {line['Page_Type'].unique()[0]}  Title: {line['Title'].unique()[0]}  Author: {author}")

            # Create dictionaries for text, commentary, and back paragraphs
            text_dic = [{"Text": text, "paragraph_ends": ends, "filename": filename} for text, ends, filename in zip(line["Text"], line["paragraph_ends"], line["filename"]) if text is not None]
            text_paragraphs = create_paragraphs_from_lines(text_dic)

            com_dic = [{"Commentary": Commentary, "paragraph_ends": ends, "index": idx, "filename": filename} for idx, Commentary, ends, filename in zip(line.index, line["Commentary"], line["paragraph_ends"], line["filename"]) if Commentary is not None]
            com_paragraphs = create_paragraphs_from_commentary(com_dic)

            back_dic = [{"Back": Back, "paragraph_ends": ends, "filename": filename} for Back, ends, filename in zip(line["Back"], line["paragraph_ends"], line["filename"]) if Back is not None]
            back_paragraphs = create_paragraphs_from_lines(back_dic) 
            
            if page_type == "root":          
                # create the root TEI file
                root_tei_document = create_root_teiCorpus(metadata)
                # get file name (for root file, it is the file_stem)
                file_name = metadata.file_stem
                section_ids = [id for id in df_revised["Title_id"].unique() if id.endswith("-00") and id != title_id]
                # log info if list is not empty, else log warning
                if section_ids:
                    logger.info(f"Section IDs to include: {section_ids}")
                    # include corpus sections with xinclude (i.e. title_ids that end with -00)
                    # sort list of section_ids 01-00, 02-00, 03-00, ..., 10-00, 11-00, ...
                    section_ids = sorted(section_ids, key=lambda x: int(x.split("-")[0]))
                    [ET.SubElement(root_tei_document, "{http://www.w3.org/2001/XInclude}include",
                                    attrib={"href": f"{metadata.file_stem}_{section_id}.xml", f"{{{xml_ns}}}xi": "http://www.w3.org/2001/XInclude"})
                    for section_id in section_ids]

                else:
                    logger.warning("No section IDs to include.")
                    # sort list before include in a copy df_revised by 01-00, 02-00, 03-00, ..., 10-00, 11-00, ...
                    df_sorted = df_revised.sort_values(by="Title_id", key=lambda x: x.str.split("-").str[0].astype(int))
                    # include individual files with xinclude == every element in df_revised["Title_id"].unique() that is not title_id
                    [ET.SubElement(root_tei_document, "{http://www.w3.org/2001/XInclude}include",
                                    attrib={"href": f"{metadata.file_stem}_{id}.xml", f"{{{xml_ns}}}xi": "http://www.w3.org/2001/XInclude"})
                                    for id in df_sorted["Title_id"].unique() if id != title_id]
                xml_str = ET.tostring(root_tei_document, encoding="utf-8")   

            elif page_type == "section":
                text_title = line["Title"].unique()[0]
                # check if there is more than two sections
                section_tei_document = create_section_tei_header(metadata, title_id, text_title, num_sections)
                file_name = f"{metadata.file_stem}_{title_id}"
                logger.info(f"Creating section TEI file for {title_id}")
                prefix = title_id.split("-")[0]
                matching_title_ids = [id for id in df_revised["Title_id"].unique() if id.startswith(prefix) and id != title_id]
                file_id = prefix

                section_number = title_id.split("-")[0]
                page_type = line["Page_Type"].unique()[0]
                if pd.notna(page_type) and page_type in last_pages_per_type:
                    vol_ends = last_pages_per_type[page_type]
                else:
                    vol_ends = ""
                image_starts = line["filename"].unique()[0]
                page_type = line["Page_Type"].unique()[0]
                if pd.notna(page_type) and page_type in last_images_per_type:
                    image_ends = last_images_per_type[page_type]
                else:
                    image_ends = ""

                logger.info(f"Section number: {section_number}, Last page: {vol_ends}, Page type: {line['Page_Type'].unique()[0]}")

                # Create the section header

                # Front Matter
                if text_paragraphs:
                    front = ET.SubElement(section_tei_document, "front")
                    div_front = ET.SubElement(front, "div", type="preface")
                    for p in text_paragraphs:
                        div_front.append(p)  # Add each <p> element to the front section

                # Include other XML files as examples
                for file_id in matching_title_ids:
                    logger.info(f"Including file {file_id} in the root TEI file")
                    xi_include = ET.SubElement(section_tei_document, "{http://www.w3.org/2001/XInclude}include", attrib={"href": f"{metadata.file_stem}_{file_id}.xml", f"{{{xml_ns}}}xi": "http://www.w3.org/2001/XInclude"})
                
                # Create ElementTree from the TEI document
                xml_str = ET.tostring(section_tei_document, encoding="utf-8")      
            
            else:
                text_title = line["Title"].unique()[0]
                file_name = f"{metadata.file_stem}_{title_id}"
                logger.info(f"Creating individual TEI file for {title_id}")
                file_id = title_id

                section_number = file[0].split("-")[0]
                page_starts = file[1]["Pages"].iloc[0]
                page_ends = file[1]["Pages"].iloc[-1]

                image_starts = file[1]["filename"].iloc[0]
                image_ends = file[1]["filename"].iloc[-1]

                # Create the TEI root element
                tei_document = create_individual_tei_header(metadata, title_id, text_title)

                text = ET.SubElement(tei_document, "text")
                pb = ET.SubElement(text, "pb")
                pb.set("facs", image_starts)

                # Create a <div> for the title section
                front = ET.SubElement(text, "front")
                div_front = ET.SubElement(front, "div", type="title")
                p = ET.SubElement(div_front, "p")
                p.text = text_title

                # Create a <body> section and a <div> for the text content
                body = ET.SubElement(text, "body")
                div_body_text = ET.SubElement(body, "div", type="text")
                p_number = 0
                for p in text_paragraphs:
                    div_body_text.append(p)  # Add each <p> element to the div part of the body
                    p_number += 1
                    seg_number = f"{metadata.file_stem}_{title_id}_{str(p_number).zfill(2)}"

                    # Convert the paragraph content to XML format
                    seg_text = "".join(ET.tostring(e, encoding="unicode") for e in list(p))

                    # Clear the text and children in the <p> element
                    p.clear()

                    # Create a new <seg> element as a child of <p>
                    seg = ET.SubElement(p, "seg")
                    seg.set(f"{{{xml_ns}}}id", seg_number)
    
                    # Parse the saved text back into XML elements and append to <seg>
                    for e in ET.fromstring(f"<root>{seg_text}</root>"):
                        seg.append(e)

                # Add commentary section if exists
                if com_paragraphs:
                    div_body_com = ET.SubElement(body, "div", type="commentary")
                    for p in com_paragraphs:
                        div_body_com.append(p[0])  # Append each commentary <p> element to the div
                        if p[1] < len(file[1]["text_para_number"]):
                            para_number = file[1]["text_para_number"].iloc[p[1]]
                            if pd.notna(para_number):
                                targe_id = int(para_number)
                            else:
                                logger.warning(f"'text_para_number' at index {p[1]} is NaN. Setting default value.")
                                targe_id = 0  # Default value for NaN index
                        else:
                            # logger.warning(f"Index {p[1]} is out-of-bounds for 'text_para_number'. Setting default value.")
                            targe_id = 0  # Default value for out-of-bounds index
                        ptr_target = f"#{metadata.file_stem}_{title_id}_{str(targe_id).zfill(2)}"
                        ptr_text = p[0].text  # Save the text of the <p> element
                        p[0].text = ""  # Clear the text in the <p> element
                        ptr = ET.SubElement(p[0], "ptr")  # Create a new <seg> element as a child of <p>
                        ptr.text = ptr_text 
                        ptr.set("target", ptr_target)

                # Add back matter section if exists
                if back_paragraphs:
                    back = ET.SubElement(text, "back")
                    # Create a <div> with the title "讚曰"
                    div_back_title = ET.SubElement(back, "div", type="title")
                    div_back_title.text = metadata.edition.back_decl_eng # Note: To check if this is correct

                    # Create a new <div> for actual back paragraphs
                    div_back_paragraphs = ET.SubElement(back, "div", type="zan")
                    for p in back_paragraphs:
                        div_back_paragraphs.append(p)
                # Create ElementTree from the TEI document
                xml_str = ET.tostring(tei_document, encoding="utf-8")           

            # Parse the XML string to get a DOM object for prettifying
            dom = parseString(xml_str)
            pretty_xml_str = dom.toprettyxml(indent="  ")

            # Save the formatted TEI XML file
            output_file_path = Path(output_dir / f"{file_name}.xml")
            with open(output_file_path, "w", encoding="UTF-8") as f:
                f.write(pretty_xml_str)

            logger.info(f"Formatted TEI XML file saved to {output_file_path}")

def create_TEI(metadata: TEIMetadata, doc_type: str = "root", title_id: str = "", text_title: str = "", num_sections: int = 0) -> ET.Element:
    """
    Create TEI element (TEI or teiCorpus) either for the root or section or text (individual) TEI document.

    Args:
        metadata (TEIMetadata): Metadata object containing information for the TEI header.
        doc_type (str): Type of the document, either "root", "section", or "text" (individual).
        title_id (str): Title ID of the document. Not needed for the root document.
        text_title (str): Title of the text. Not needed for the root document.
        num_sections (int): Number of sections in the document. Only needed for the section document.
    Returns:
        ET.Element: The root element of the TEI document.
    """
    file_name = f"{metadata.file_stem}.xml" if doc_type == "root" else f"{metadata.file_stem}_{title_id}.xml"

    if doc_type == "root" or doc_type == "section":
        tei_element = "teiCorpus"
        version = "5.0"
    else: # individual file
        tei_element = "TEI"
        version = "3.3.0"

    # Create the root element with namespaces for TEI and XInclude using f-string
    TEI = ET.Element(tei_element, 
                        nsmap={"tei": tei_ns, "xi": xi_ns, "xml": xml_ns},
                        attrib={f"{{{xml_ns}}}id": file_name},
                        version=version)

    # if section, add prev=f"{metadata.file_stem}.xml" attribute to TEI element
    if doc_type != "root":
        if doc_type == "section":
            previous_file = f"{metadata.file_stem}.xml"
        elif doc_type == "text":
            # if title_id = 01-01, prev = 01-00.xml
            doc = re.sub(r"-\d{2}$", "-00", title_id)
            previous_file = f"{metadata.file_stem}_{doc}.xml"
        
        TEI.set("prev", previous_file)

    # teiCorpus / TEI > teiHeader
    teiHeader = ET.SubElement(TEI, "teiHeader")
    
    # differentiate dig_roles and trad_roles in metadata.contributors
    dig_roles = [contrib for contrib in metadata.contributors if isinstance(contrib, DigRole)]
    trad_roles = [contrib for contrib in metadata.contributors if isinstance(contrib, TradRole)]

    def attach_resp(branch: ET.Element, role_list: List[Union[DigRole, TradRole]]):
        """
        Add contributors information to its branch.
        """
        for contrib in role_list:
            # create a new <respStmt> element for each contributor
            respStmt = ET.SubElement(branch, "respStmt")
            ET.SubElement(respStmt, "resp").text = contrib.dig_role if isinstance(contrib, DigRole) else contrib.trad_role
            persName = ET.SubElement(respStmt, "persName")
            persNameAlph = ET.SubElement(persName, "persName", attrib={f"{{{xml_ns}}}lang": contrib.alph_lang})
            ET.SubElement(persNameAlph, "forename").text = contrib.forename_alph
            ET.SubElement(persNameAlph, "surname").text = contrib.surname_alph
            persNameOrig = ET.SubElement(persName, "persName", attrib={f"{{{xml_ns}}}lang": contrib.orig_lang})
            ET.SubElement(persNameOrig, "forename").text = contrib.forename_orig
            ET.SubElement(persNameOrig, "surname").text = contrib.surname_orig
            ET.SubElement(persName, "idno", type=contrib.idno_type).text = contrib.idno
        return respStmt
    
    # teiCorpus / TEI  > teiHeader > fileDesc
    fileDesc = ET.SubElement(teiHeader, "fileDesc")

    # [...] fileDesc > titleStmt
    titleStmt = ET.SubElement(fileDesc, "titleStmt")

    # todo: instead of using is_root, use doc_type for all other conditions
    if doc_type == "root":
        title_main = f"{metadata.title_main}（电子版)"
        title_alt =  f"{metadata.title_alt}（電子版)"
    else: # section or individual file
        text_alt = "PLACEHOLDER"
        if doc_type == "section":
            cn_numerals = r"[一二三四五六七八九十百千万]"
            chapter_specificities = r"[上中下]" # up = 上, middle = 中, down = 下
            chapter_up_middle_down_pattern = fr"卷{chapter_specificities}$"
            chapter_numeral_pattern = fr"卷{cn_numerals}+$"
            # if title_id ends with chapter pattern, take only the pattern
            if re.search(chapter_up_middle_down_pattern, text_title):
                text_title = re.search(chapter_up_middle_down_pattern, text_title).group()
                # 卷上 = Chapter 1
                if text_title.endswith("上"):
                    text_alt = "Chapter 1"
                # 卷中 = Chapter 2
                elif text_title.endswith("中"):
                    text_alt = "Chapter 2"
                else:
                    if num_sections == 2:
                        text_alt = "Chapter 2"
                    else:
                        text_alt = "Chapter 3"
            elif re.search(chapter_numeral_pattern, text_title):
                text_title = re.search(chapter_numeral_pattern, text_title).group()
                # first replace 卷 with Chapter
                text_alt = re.sub(r"卷", "Chapter ", text_title)
                text_alt = re.sub(fr"{cn_numerals}+", lambda x: str(chinese_num_to_integer(x.group())), text_alt)
            else:
                # raise ValueError(f"Text title {text_title} does not end with a chapter pattern.")
                text_title = text_title
                text_alt = pypinyin.slug(text_title, separator="-", style=pypinyin.Style.NORMAL, errors="ignore")
        else: # individual file
            # transliterate the traditional Chinese text title with pinyin
            text_alt = pypinyin.slug(text_title, separator="-", style=pypinyin.Style.NORMAL, errors="ignore") if text_title else ""

        title_main = f"{metadata.title_main}: {text_title}（电子版)"
        title_alt = f"{metadata.title_alt}: {text_alt} (An electronic version)"

    ET.SubElement(titleStmt, "title", type="main", attrib={f"{{{xml_ns}}}lang": metadata.title_main_lang}).text = title_main
    ET.SubElement(titleStmt, "title", type="alt", attrib={f"{{{xml_ns}}}lang": metadata.title_alt_lang}).text = title_alt
    if doc_type == "root": # add short titles only for root document
        ET.SubElement(titleStmt, "title", type="short", attrib={f"{{{xml_ns}}}lang": metadata.title_main_lang}).text = metadata.title_main_short
        ET.SubElement(titleStmt, "title", type="short", attrib={f"{{{xml_ns}}}lang": metadata.title_alt_lang}).text = metadata.title_alt_short
    attach_resp(titleStmt, metadata.contributors) # attach all contributors to titleStmt

    # [...] fileDesc > editionStmt
    editionStmt = ET.SubElement(fileDesc, "editionStmt")
    edition = ET.SubElement(editionStmt, "edition")
    for funder in metadata.edition.funder:
        ET.SubElement(edition, "funder").text = funder
    attach_resp(editionStmt, dig_roles) # attach Digital contributors to editionStmt

    publicationStmt = ET.SubElement(fileDesc, "publicationStmt")
    for authority in metadata.edition.authorities:
        ET.SubElement(publicationStmt, "authority").text = authority
    ET.SubElement(publicationStmt, "pubPlace").text = metadata.edition.pub_place
    ET.SubElement(publicationStmt, "date").text = str(metadata.edition.pub_date)
    ET.SubElement(publicationStmt, "idno", type="DOI").text = metadata.edition.DOI
    availability = ET.SubElement(publicationStmt, "availability", status="restricted")
    ET.SubElement(availability, "licence", target=metadata.edition.license_target).text = metadata.edition.license

    sourceDesc = ET.SubElement(fileDesc, "sourceDesc")
    listPerson = ET.SubElement(sourceDesc, "listPerson")
    for contrib in trad_roles:
        person = ET.SubElement(listPerson, "person", role=contrib.trad_role)
        persName = ET.SubElement(person, "persName", ref=contrib.idno)
        ET.SubElement(persName, "forename").text = contrib.forename_alph
        ET.SubElement(persName, "surname").text = contrib.surname_alph
        if contrib.forename_orig and contrib.surname_orig:
            persNameOrig = ET.SubElement(persName, "persName", attrib={f"{{{xml_ns}}}lang": contrib.orig_lang})
            ET.SubElement(persNameOrig, "forename").text = contrib.forename_orig
            ET.SubElement(persNameOrig, "surname").text = contrib.surname_orig
        ET.SubElement(persName, "idno", type=contrib.idno_type).text = contrib.idno

    listBibl = ET.SubElement(sourceDesc, "listBibl")
    ET.SubElement(listBibl, "head").text = "Reference Work"
    biblStruct = ET.SubElement(listBibl, "biblStruct")
    analytic = ET.SubElement(biblStruct, "analytic")
    ET.SubElement(analytic, "title", level="a", attrib={f"{{{xml_ns}}}lang": metadata.title_main_lang}).text = metadata.title_main

    monogr = ET.SubElement(biblStruct, "monogr")
    ET.SubElement(monogr, "title", level="s", attrib={f"{{{xml_ns}}}lang": metadata.title_main_lang}).text = metadata.title_main
    ET.SubElement(monogr, "edition").text = metadata.edition.license
    imprint = ET.SubElement(monogr, "imprint")

    ET.SubElement(imprint, "publisher").text = metadata.source.source_publisher
    for pub_place in metadata.source.source_pub_place:
        ET.SubElement(imprint, "pubPlace").text = pub_place
    ET.SubElement(imprint, "date", when=str(metadata.source.source_date_conv)).text = str(metadata.source.source_date_trad)
    ET.SubElement(sourceDesc, "p", attrib={f"{{{xml_ns}}}lang": trad_roles[0].alph_lang}).text = metadata.source.source_description_eng
    ET.SubElement(sourceDesc, "p", attrib={f"{{{xml_ns}}}lang": trad_roles[0].orig_lang}).text = metadata.source.source_description_chin

    if doc_type == "root": # encoding description only for root document
        # teiCorpus > teiHeader > encodingDesc
        encodingDesc = ET.SubElement(teiHeader, "encodingDesc")

        # childs of encodingDesc
        editorialDecl = ET.SubElement(encodingDesc, "editorialDecl")
        tagsDecl = ET.SubElement(encodingDesc, "tagsDecl")
        classDeclTextParts = ET.SubElement(encodingDesc, "classDecl", attrib={"type": "text-parts"})
        classDeclCreatorStatus = ET.SubElement(encodingDesc, "classDecl", attrib={"type": "creator-status"})
        classDeclTextType = ET.SubElement(encodingDesc, "classDecl", attrib={"type": "text-type"})
        appInfo = ET.SubElement(encodingDesc, "appInfo")
        profileDesc = ET.SubElement(teiHeader, "profileDesc")

        # [...] encodingDesc > editorialDecl
        ET.SubElement(editorialDecl, "p", attrib={f"{{{xml_ns}}}lang": metadata.title_alt_lang}).text = metadata.edition.editorial_decl_eng
        correction = ET.SubElement(editorialDecl, "correction", attrib={"status": metadata.edition.cor_status, f"{{{xml_ns}}}lang": metadata.title_alt_lang})
        ET.SubElement(correction, "p").text = metadata.edition.cor_presentation_eng
        punctuation = ET.SubElement(editorialDecl, "punctuation")
        ET.SubElement(punctuation, "p", attrib={f"{{{xml_ns}}}lang": metadata.title_alt_lang}).text = metadata.edition.punct_decl_eng
        quotation = ET.SubElement(editorialDecl, "quotation")
        ET.SubElement(quotation, "p", attrib={f"{{{xml_ns}}}lang": metadata.title_alt_lang}).text = metadata.edition.quote_decl_eng

        # [...] encodingDesc > tagsDecl
        namespace = ET.SubElement(tagsDecl, "namespace", name=tei_ns) # TEI namespace
        ET.SubElement(namespace, "tagUsage", gi="front").text = metadata.edition.front_decl_eng
        ET.SubElement(namespace, "tagUsage", gi="div").text = metadata.edition.div_decl_eng
        ET.SubElement(namespace, "tagUsage", gi="seg").text = metadata.edition.seg_decl_eng
        ET.SubElement(namespace, "tagUsage", gi="lb").text = metadata.edition.lb_decl_eng
        ET.SubElement(namespace, "tagUsage", gi="back").text = metadata.edition.back_decl_eng

        # [...] encodingDesc > classDecl (*multiple)
        classDeclTextParts_taxonomy = ET.SubElement(classDeclTextParts, "taxonomy")
        category_title = ET.SubElement(classDeclTextParts_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "title"})
        ET.SubElement(category_title, "catDesc").text = "Title"
        category_text = ET.SubElement(classDeclTextParts_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "text"})
        ET.SubElement(category_text, "catDesc").text = "Base text"
        category_commentaries = ET.SubElement(classDeclTextParts_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "commentaries"})
        ET.SubElement(category_commentaries, "catDesc").text = "Commentaries"
        category_commentary = ET.SubElement(classDeclTextParts_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "commentary"})
        ET.SubElement(category_commentary, "catDesc").text = "Commentary"
        category_subcommentary = ET.SubElement(classDeclTextParts_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "subcommentary"})
        ET.SubElement(category_subcommentary, "catDesc").text = "Subcommentary"
        classDeclCreatorStatus_taxonomy = ET.SubElement(classDeclCreatorStatus, "taxonomy")
        category_author = ET.SubElement(classDeclCreatorStatus_taxonomy, "category")
        category_author.text = "author"
        ET.SubElement(category_author, "catDesc").text = "The author of the text (the person who wrote the text)"
        category_editor = ET.SubElement(classDeclCreatorStatus_taxonomy, "category")
        category_editor.text = "editor"
        ET.SubElement(category_editor, "catDesc").text = "The person edited (prepared a version) of the text, sometimes adding a preface, reorganizing the text, emending or commenting it"
        category_compilor = ET.SubElement(classDeclCreatorStatus_taxonomy, "category")
        category_compilor.text = "compilor"
        ET.SubElement(category_compilor, "catDesc").text = "The person who compiled the book, made of different texts (an anthology for example)"
        classDeclTextType_taxonomy = ET.SubElement(classDeclTextType, "taxonomy")
        category_corpus = ET.SubElement(classDeclTextType_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "corpus"})
        ET.SubElement(category_corpus, "catDesc").text = "For a book or a section that is made of different components"
        category_lexicon = ET.SubElement(classDeclTextType_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "lexicon"})
        ET.SubElement(category_lexicon, "catDesc").text = "For dictionaries and encyclopedias"
        category_prose = ET.SubElement(classDeclTextType_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "prose"})
        ET.SubElement(category_prose, "catDesc").text = "For essays, letters and other genres in prose"
        category_verse = ET.SubElement(classDeclTextType_taxonomy, "category", attrib={f"{{{xml_ns}}}id": "verse"})
        ET.SubElement(category_verse, "catDesc").text = "For poetic genres including verse"    
        
        # [...] encodingDesc > appInfo
        application = ET.SubElement(appInfo, "application", version=metadata.app_info.app_version, ident=metadata.app_info.app_id)\
                        if metadata.app_info.app_id else ET.SubElement(appInfo, "application", version=metadata.app_info.app_version)
        ET.SubElement(application, "label").text = metadata.app_info.app_label
        ET.SubElement(application, "ptr", target=metadata.app_info.app_target)

        # [...] encodingDesc > profileDesc
        langUsage = ET.SubElement(profileDesc, "langUsage")
        lang_dict = {"lzh": "Literary Chinese", "och": "Old Chinese", "ltc": "Late Middle Chinese", "en": "English", "fr": "French"}
        for lang_id, lang_name in lang_dict.items():
            ET.SubElement(langUsage, "language", ident=lang_id).text = lang_name
        textClass = ET.SubElement(profileDesc, "textClass")
        ET.SubElement(textClass, "catRef", target=metadata.source.style)
    elif doc_type == "text": # profile description shorter than "root" only for individual document
        profileDesc = ET.SubElement(teiHeader, "profileDesc")
        textClass = ET.SubElement(profileDesc, "textClass")
        ET.SubElement(textClass, "catRef", target=metadata.source.style)
    return TEI

def create_root_teiCorpus(metadata: TEIMetadata) -> ET.Element:
    """
    Create the root teiCorpus element for the TEI document.

    Args:
        metadata (TEIMetadata): Metadata object containing information for the TEI header.

    Returns:
        ET.Element: The root teiCorpus element of the TEI document.
    """
    return create_TEI(metadata, doc_type="root")

def create_section_tei_header(metadata: TEIMetadata, title_id: str, text_title: str, num_sections: int) -> ET.Element:
    """
    Create the TEI header element for a section document.

    Args:
        metadata (TEIMetadata): Metadata object containing information for the TEI header.
        title_id (str): Title ID of the section document.
        text_title (str): Title of the text.
        num_sections (int): Number of sections in the document.

    Returns:
        ET.Element: The root teiCorpus element of the TEI document.
    """
    return create_TEI(metadata, doc_type="section", title_id = title_id, text_title = text_title, num_sections = num_sections)

def create_individual_tei_header(metadata: TEIMetadata, title_id: str, text_title: str) -> ET.Element:
    """
    Create the TEI header element for an individual document.

    Args:
        metadata (TEIMetadata): Metadata object containing information for the TEI header.
        title_id (str): Title ID of the section document.
        text_title (str): Title of the text.

    Returns:
        ET.Element: The TEI header element for the document.
    """
    return create_TEI(metadata, doc_type="text", title_id = title_id, text_title = text_title)

def prettify_xml(tree: ET.ElementTree, indentation: str = "    ") -> str:
    """
    Prettify the XML content of an ElementTree.

    Args:
        tree: ElementTree instance to be prettified.

    Returns:
        str: Prettified XML content as a string.
        indentation: Indent for the XML.
    """
    # Get the XML content as a string
    xml_str = ET.tostring(tree.getroot(), encoding="utf-8").decode("utf-8")

    # Parse the XML content
    xml_content = parseString(xml_str)

    # Pretty print the XML content
    pretty_xml = xml_content.toprettyxml(indent=indentation)

    return pretty_xml

def get_first_non_none(*args: Any) -> Optional[Any]:
    """
    Returns the first non-None value from the given arguments.

    Args:
        *args: Variable length argument list.

    Returns:
        Optional[Any]: The first argument that is not None, or None if all arguments are None.
    """
    for arg in args:
        if arg is not None:
            return arg
    return None  # Return None if all arguments are None

def create_paragraphs_from_lines(lines: List[Dict[str, Any]]) -> List[ET.Element]:
    """
    Create paragraphs from a list of lines.

    Args:
        lines: List of lines, each line represented as a dictionary containing "Text", "Back", "filename", and "paragraph_ends".
    """
    paragraphs = []  # Used to store generated <p> elements and page breaks
    current_paragraph_elements = []  # Accumulates elements for the current paragraph
    last_filename = None  # Track the last processed filename

    for line in lines:
        filename = line.get("filename")
        if line.get("Text") is not None:
            text = line.get("Text")
        else:
            text = line.get("Back")

        # When filename changes, directly add the <pb> tag in the accumulated elements
        if filename != last_filename and filename is not None:
            pb_tag = ET.Element("pb", attrib={"facs": filename})  # Create <pb> tag as an element
            current_paragraph_elements.append(pb_tag)  # Add <pb> element to current paragraph elements
            last_filename = filename  # Update last processed filename

        if text:  # Ensure the text is not None
            text_element = ET.Element("lb")  # Use a line element to contain text
            text_element.text = text

            # Replace <it> with <hi rend="italic">
            text_element.text = re.sub(r'<it>(.*?)<\/it>', r'<hi rend="italic">\1<\/hi>', text_element.text)
            current_paragraph_elements.append(text_element)

        if line.get("paragraph_ends", False):
            # The current line marks the end of a paragraph, create <p> element
            p_element = ET.Element("p")
            p_element.extend(current_paragraph_elements)  # Add all accumulated elements to the <p> element
            paragraphs.append(p_element)
            current_paragraph_elements = []  # Reset current paragraph elements

    # Handle the last paragraph if it wasn't explicitly marked as ending
    if current_paragraph_elements:
        p_element = ET.Element("p")
        p_element.extend(current_paragraph_elements)
        paragraphs.append(p_element)

    return paragraphs

def create_paragraphs_from_commentary(lines: List[Dict[str, Any]]) -> List[Tuple[ET.Element, int]]:
    """
    Create paragraphs of commentary from a list of lines.

    Args:
        lines: List of lines, each line represented as a dictionary containing "Commentary" and "index".

    Returns:
        List[Tuple[ET.Element, int]]: List of tuples containing paragraph elements and the last line index before the paragraph.
    """
    paragraphs = []  # Used to store generated <p> elements and their related most recent non-empty text_para_number
    current_paragraph_text = []  # Used to accumulate the current paragraph text
    last_line_index_before_paragraph = None  # Most recent line index corresponding to the non-empty text_para_number
    previous_index = None  # Index of the previous line, used to detect paragraph breaks

    for line in lines:
        commentary_text = line.get("Commentary")
        current_index = line.get("index")  # Current line index

        # Detect paragraph breaks: if the current index is not continuous with the previous index, it is considered a new paragraph
        if previous_index is not None and (current_index - previous_index > 1):
            # End the current paragraph and start a new one
            if current_paragraph_text:
                p_element = ET.Element("p")
                p_element.text = "".join(current_paragraph_text)
                paragraphs.append((p_element, last_line_index_before_paragraph))
                current_paragraph_text = []  # Reset current paragraph text

        if commentary_text:
            # Replace <it> with <hi rend="italic">
            commentary_text = re.sub(r'<it>(.*?)<\/it>', r'<hi rend="italic">\1<\/hi>', commentary_text)
            current_paragraph_text.append(commentary_text)
            # Update last_line_index_before_paragraph only when starting to collect new paragraph text
            if not current_paragraph_text[:-1]:  # Check if it's the first line of the paragraph
                last_line_index_before_paragraph = current_index - 1

        previous_index = current_index  # Update the previous line index to the current line index

    # Check and process the remaining paragraph text
    if current_paragraph_text:
        p_element = ET.Element("p")
        p_element.text = "".join(current_paragraph_text)
        paragraphs.append((p_element, last_line_index_before_paragraph))

    return paragraphs

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate TEI files from a DataFrame.")
    parser.add_argument("input_file", type=str, help="Path to the input CSV file containing the data.")
    parser.add_argument("metadata_file", type=str, help="Path to the YAML file containing the TEI metadata.")
    parser.add_argument("output_dir", type=str, help="Path to the output directory.")
    parser.add_argument("--verbose", action="store_true", default=False, help="Set logger to verbose mode.")

    args = parser.parse_args()
    # Read input DataFrame
    df = pd.read_csv(args.input_file)

    # Read metadata
    metadata = parse_yaml_to_metadata(args.metadata_file)

    # Create output directory if it doesn't exist
    output_dir = Path(args.output_dir)
    output_dir.mkdir(parents=True, exist_ok=True)

    # Initialize TEIFileGenerator and generate TEI files
    tei_generator = TEIFileGenerator(logger_verbose=args.verbose)
    tei_generator.generate_tei_files(df, metadata, output_dir)
