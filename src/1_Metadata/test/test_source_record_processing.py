import sys
import unittest
from loguru import logger
from pathlib import Path
sys.path.append(str(src := Path(__file__).resolve().parents[2])) # add the src directory to sys.path
from MetadataGeneration.process_source_record import parse_text

# Line from the ZBZ source record to test the parsing function (box number 15)
sample_text = """
327    0#$a第十五函: 新唐書糾謬 二十卷, 坿修唐書史臣表一卷 / [宋] 吳縝 撰, [清] 錢大昕 撰修唐書史臣表, 洞霄圖志 六卷 / [宋] 鄧牧 撰, 聱隅子歔欷瑣微論 二卷 / [宋] 黃晞 撰, 世緯 二卷 / [明] 袁衮 撰 
"""

# Expected results for the test case
expected_results = [
    {"title": "新唐書糾謬", "volume": "二十卷", "dynasty": "", "author": "", "function": ""},
    {"title": "坿修唐書史臣表", "volume": "一卷", "dynasty": "宋", "author": "吳縝", "function": "撰"},
    {"title": "唐書史臣表", "volume": "", "dynasty": "", "author": "錢大昕", "function": "撰, 修"},
    {"title": "洞霄圖志", "volume": "六卷", "dynasty": "宋", "author": "鄧牧", "function": "撰"},
    {"title": "聱隅子歔欷瑣微論", "volume": "二卷", "dynasty": "宋", "author": "黃晞", "function": "撰"},
    {"title": "世緯", "volume": "二卷", "dynasty": "明", "author": "袁衮", "function": "撰"}
]

class TestSourceRecordProcessing(unittest.TestCase):
    def test_parse_text(self):
        """
        Test the parse_text function to ensure it extracts the correct information.
        """
        parsed_results = parse_text(sample_text)
        
        for i, item in enumerate(parsed_results):
            for key in expected_results[i]:
                with self.subTest(i=i, key=key):
                    if item[key] == expected_results[i][key]:
                        logger.info(f"Test {i+1} for {key} passed: {item[key]}")
                    else:
                        logger.error(f"Test {i+1} for {key} failed: expected {expected_results[i][key]} but got {item[key]}")
                    self.assertEqual(item[key], expected_results[i][key])

    def test_global_precision_recall(self):
        """
        Calculate and test the global precision and recall.
        """
        parsed_results = parse_text(sample_text)

        # Initialize variables for global precision and recall
        TP_global = 0
        FP_global = 0
        FN_global = 0

        # Precision and recall for each category
        precision_recall_dict = {}

        for i, item in enumerate(parsed_results):
            for key in expected_results[i]:
                if key not in precision_recall_dict:
                    precision_recall_dict[key] = {"TP": 0, "FP": 0, "FN": 0}
                if item[key] == expected_results[i][key]:
                    precision_recall_dict[key]["TP"] += 1
                else:
                    if item[key]:  # Predicted but incorrect
                        precision_recall_dict[key]["FP"] += 1
                    if expected_results[i][key]:  # Expected but not predicted correctly
                        precision_recall_dict[key]["FN"] += 1

                # Update global counters
                TP_global += 1 if item[key] == expected_results[i][key] else 0
                FP_global += 1 if (item[key] and item[key] != expected_results[i][key]) else 0
                FN_global += 1 if (expected_results[i][key] and item[key] != expected_results[i][key]) else 0

        # Calculate precision and recall for each category
        for key in precision_recall_dict:
            TP = precision_recall_dict[key]["TP"]
            FP = precision_recall_dict[key]["FP"]
            FN = precision_recall_dict[key]["FN"]
            precision = TP / (TP + FP) if (TP + FP) != 0 else 0
            recall = TP / (TP + FN) if (TP + FN) != 0 else 0
            precision_recall_dict[key]["precision"] = round(precision, 2)
            precision_recall_dict[key]["recall"] = round(recall, 2)

        # Calculate global precision and recall
        global_precision = TP_global / (TP_global + FP_global) if (TP_global + FP_global) != 0 else 0
        global_recall = TP_global / (TP_global + FN_global) if (TP_global + FN_global) != 0 else 0

        logger.info("\nCategory-wise Precision and Recall:")
        for key, values in precision_recall_dict.items():
            logger.info(f"Category: {key}")
            logger.info(f"  Precision: {values['precision']}")
            logger.info(f"  Recall: {values['recall']}")

        logger.info("\nGlobal Precision and Recall:")
        logger.info(f"  Global Precision: {round(global_precision, 2)}")
        logger.info(f"  Global Recall: {round(global_recall, 2)}")

        # Assertions for global precision and recall
        self.assertGreaterEqual(global_precision, 0.8)
        self.assertGreaterEqual(global_recall, 0.8)

if __name__ == "__main__":
    unittest.main()
