import argparse
import csv
import re
import sys
from pathlib import Path
from typing import Dict, List, Tuple, Any
from loguru import logger # add the src directory to sys.path
sys.path.append(str(src := Path(__file__).resolve().parents[1]))
from utils.numeral_conversion import chinese_num_to_integer, CN_NUM, CN_UNIT

# Chinese numerals and units
cn_num = list(CN_NUM.keys()) + list(CN_UNIT.keys())

# All regular expressions used for parsing the source record
box_number_pattern = re.compile(r"([一二三四五六七八九十]+)函:")
box_information_pattern = re.compile(fr"{box_number_pattern.pattern}(.*?)(?={box_number_pattern.pattern}|\Z)", re.DOTALL)
volume_pattern = re.compile(rf"([{''.join(cn_num)}]+卷)")
dynasty_pattern = re.compile(r"\[([^\]]+)\]")
author_pattern = re.compile(r" ([\u4e00-\u9fa5]+) (撰|修)")

def parse_text(text: str) -> List[Dict[str, Any]]:
    """
    Parse the given text to extract titles, volumes, dynasties, authors, and their functions.

    Args:
        text (str): The text to parse.

    Returns:
        List[Dict[str, Any]]: A list of dictionaries containing the parsed information.
    """
    entries = text.split(", ")
    results = []

    for entry in entries:
        result = {"title": "", "volume": "", "dynasty": "", "author": "", "function": ""}

        # Extract title and volume
        title_volume_match = re.search(rf"([\u4e00-\u9fa5]+) ({volume_pattern.pattern})", entry)
        if title_volume_match:
            result["title"] = title_volume_match.group(1)
            result["volume"] = title_volume_match.group(2)
        else:
            title_only_match = re.match(r"([\u4e00-\u9fa5]+)", entry)
            if title_only_match:
                result["title"] = title_only_match.group(1)

        # Extract dynasties
        dynasties = dynasty_pattern.findall(entry)
        if dynasties:
            result["dynasty"] = ", ".join(dynasties)

        # Extract authors and functions
        authors_funcs = author_pattern.findall(entry)
        if authors_funcs:
            authors = []
            functions = []
            for author, func in authors_funcs:
                authors.append(author)
                functions.append(func)
            result["author"] = ", ".join(authors)
            result["function"] = ", ".join(functions)

        results.append(result)
    
    return results

def process_source_record(file_path: Path, output_dir: Path) -> Dict[int, List[Dict[str, Any]]]:
    """
    Process the source record file and extract titles, volumes, dynasties, authors, and status for each box number.
    Write the extracted records to CSV files in the output directory.

    Args:
        file_path (Path): Path to the source record text file.
        output_dir (Path): Directory where the output CSV files will be stored.

    Returns:
        Dict[int, List[Dict[str, Any]]]: A dictionary where keys are box numbers and values are lists of dictionaries
                                         containing title, volume number, dynasty, author, and status.
    """
    # Initialize dictionary to store box records
    box_records: Dict[int, List[Dict[str, Any]]] = {}
    
    logger.info(f"Processing file: {file_path}")
    
    with open(file_path, "r", encoding="utf-8") as file:
        file_content = file.read()
        logger.debug(f"File content loaded, length: {len(file_content)}")
        
        # Find all matches of box number and content
        for match in box_information_pattern.finditer(file_content):
            cn_box_number = match.group(1)  # Group 1 is the Chinese numeral box number
            box_number = chinese_num_to_integer(cn_box_number)
            content = match.group(2).strip()  # Group 2 is the content of the box
            logger.debug(f"Box number: {box_number}, content length: {len(content)}")
            
            # Parse the content to extract records
            records = parse_text(content)
            
            if records:
                box_records[box_number] = records

    logger.info(f"Total boxes processed: {len(box_records)}")
    
    # Write the extracted records to CSV files
    output_dir.mkdir(parents=True, exist_ok=True)
    for box_number, records in box_records.items():
        output_file = output_dir / f"box_{box_number}.csv"
        with open(output_file, "w", encoding="utf-8", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=["title", "volume", "dynasty", "author", "function"])
            writer.writeheader()
            writer.writerows(records)
        logger.info(f"Processed records for box {box_number} written to: {output_file}")

    return box_records

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process source record file and extract titles, dynasties, and authors.")
    
    parser.add_argument("file_path", type=Path, help="Path to the source record text file.")
    parser.add_argument("output_dir", type=Path, help="Directory where the output CSV files will be stored.")
    parser.add_argument("--log-level", type=str, default="INFO", help="Logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)")

    if len(sys.argv) > 1:
        args = parser.parse_args()
    else:
        # Use default values if no command-line arguments are provided
        class Args:
            project = src.parents[0]
            file_path = project / "data/ZBZ/sourceRecord.txt"
            output_dir = project / "data/ZBZ/processed_records"
            log_level = "INFO"
        args = Args()
    
    logger.remove()
    logger.add(sys.stdout, level=args.log_level)  # Use sys.stdout for console logging

    box_records = process_source_record(args.file_path, args.output_dir)
