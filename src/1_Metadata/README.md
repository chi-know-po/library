# Metadata

## Description

This folder gathers scripts necessary to generate metadata for each corpus.   
These aims to facilitate the Editor task to create the necessary inputs for the Alto to TEI pipeline from `src/Edition/`.

The `parse_text` function (**`process_source_record.py`**) extracts titles, volumes, dynasties, authors, and their functions from the source record file.  
Its test case (**`test/test_source_record_processing.py`**) includes a sample text from the ZBZ source record and the expected results for parsing this text. The test case checks if the function correctly extracts the information and calculates the global precision and recall for the extracted data.

## Structure

- Create Metadatada (**`1_Create_Metadata_csv.ipynb`**)  

    This notebook agregates file names and corresponding metadata for all parts of the book that is being edited (general structure of the book).  
        **Input**:  
            - `TITLE.yaml` (YAML metadata file)  
            - `Altos/`  
        **Output**:  
            - `TITLE_metadata.csv`  
        **/!\ Recommended verification:** The metadata file can contain small structure mistakes, if so correct them and generate another file that ends with `_ok.csv`.

- Additional scripts: **Source record Processing**
  - **`process_source_record.py`**: Script to process the source record file and extract titles, dynasties, authors and their functions for each box number.
  - **`test/test_source_record_processing.py`**: Unit tests for the `parse_text` function from the `process_source_record.py` script.
